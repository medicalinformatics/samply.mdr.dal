/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.SlotDAO;
import de.samply.sdao.DAOException;

public class DataElementDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        MDRConnection mdr = MDRTestSuite.get();
        DefinitionDAO defDao = mdr.get(DefinitionDAO.class);
        ElementDAO dao = mdr.get(ElementDAO.class);
        ScopedIdentifierDAO scdao = mdr.get(ScopedIdentifierDAO.class);

        DescribedValueDomain ds = new DescribedValueDomain();
        ds.setDescription("\\d+");
        ds.setDatatype("integer");
        ds.setFormat("none");
        ds.setMaxCharacters(4);
        ds.setUnitOfMeasure("kg");
        ds.setValidationType(ValidationType.NONE);

        dao.saveElement(ds);
        assertTrue(dao.getElement(ds.getId()).getId() == ds.getId());
        assertTrue(dao.getElement(ds.getId()) instanceof DescribedValueDomain);

        DataElement element = new DataElement();
        element.setValueDomainId(ds.getId());

        dao.saveElement(element);
        assertTrue(dao.getElement(element.getId()).equals(element));

        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setElementId(element.getId());
        identifier.setStatus(Status.RELEASED);
        identifier.setNamespaceId(dao.getNamespaces().get(0).getId());
        identifier.setNamespace("mdr-test");
        identifier.setUrl("https://none.org");
        identifier.setVersion("1.0a");
        identifier.setIdentifier("" + element.getId());
        identifier.setElementType(ElementType.DATAELEMENT);
        identifier.setCreatedBy(1);

        scdao.saveScopedIdentifier(identifier);

        Definition d = new Definition();
        d.setElementId(element.getId());
        d.setDefinition("Das Gewicht des Babys zum Zeitpunkt der Geburts");
        d.setDesignation("Gewicht");
        d.setLanguage("de");
        d.setScopedIdentifierId(identifier.getId());

        defDao.saveDefinition(d);

        Definition d2 = new Definition();
        d2.setElementId(element.getId());
        d2.setDefinition("The babys weight at time of birth");
        d2.setDesignation("Weight");
        d2.setLanguage("en");
        d2.setScopedIdentifierId(identifier.getId());

        defDao.saveDefinition(d2);

        Definition d3 = new Definition();
        d3.setElementId(ds.getId());
        d3.setDefinition("The patients weight in kg");
        d3.setLanguage("en");
        d3.setDesignation("Weight");
        d3.setScopedIdentifierId(identifier.getId());

        defDao.saveDefinition(d3);


        d3 = new Definition();
        d3.setElementId(ds.getId());
        d3.setDefinition("Das Gewicht des Patienten in kg");
        d3.setLanguage("de");
        d3.setDesignation("Gewicht");
        d3.setScopedIdentifierId(identifier.getId());

        defDao.saveDefinition(d3);


        assertTrue(defDao.getDefinition(d.getId()).equals(d));

        List<Definition> designations = defDao.getDefinitions(element.getId(), identifier.getId());
        assertTrue(designations.size() == 2);
        assertTrue(designations.contains(d));
        assertTrue(designations.contains(d2));

        IdentifiedElement fullElement = mdr.get(IdentifiedDAO.class).getElement(identifier.toString());
        assertTrue(fullElement.getDefinitions().contains(d));
        assertTrue(fullElement.getDefinitions().contains(d2));

        ScopedIdentifier newIdentifier = new ScopedIdentifier();
        newIdentifier.setCreatedBy(1);
        newIdentifier.setElementId(element.getId());
        newIdentifier.setStatus(Status.RELEASED);
        newIdentifier.setNamespaceId(dao.getNamespaces().get(0).getId());
        newIdentifier.setNamespace("mdr-test");
        newIdentifier.setUrl("https://none.org");
        newIdentifier.setVersion("1.1a");
        newIdentifier.setIdentifier("" + element.getId());
        newIdentifier.setElementType(ElementType.DATAELEMENT);
        newIdentifier.setCreatedBy(1);

        scdao.saveScopedIdentifier(newIdentifier);

        fullElement = mdr.get(IdentifiedDAO.class).getElement(identifier.toString());
        assertTrue(fullElement.getDefinitions().contains(d));
        assertTrue(fullElement.getDefinitions().contains(d2));

        fullElement = mdr.get(IdentifiedDAO.class).getElement(newIdentifier.toString());
        assertTrue(fullElement.getDefinitions().size() == 0);

        Slot s = new Slot();
        s.setKey("ADT_ID");
        s.setValue("ADT_TEST_ID_456");
        s.setScopedIdentifierId(newIdentifier.getId());

        SlotDAO sDao = mdr.get(SlotDAO.class);
        sDao.saveSlot(s);

        HashMap<String, String> slotValues = new HashMap<>();
        slotValues.put("ADT_ID", "ADT_TEST_ID_456");

        List<IdentifiedElement> found = mdr.get(IdentifiedDAO.class).findElements("", newIdentifier.getNamespace(), new ElementType[] {ElementType.DATAELEMENT},
                new Status[] {Status.RELEASED}, slotValues);

        assertTrue(found.size() > 0);
        assertTrue(found.get(0).getElement().getId() == element.getId());

        commit(mdr);

        mdr.close();
    }

}
