/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.sdao.DAOException;

public class ScopedIdentifierDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        MDRConnection mdr = MDRTestSuite.get();
        ScopedIdentifierDAO dao = mdr.get(ScopedIdentifierDAO.class);
        ElementDAO edao = mdr.get(ElementDAO.class);

        DescribedValueDomain domain = new DescribedValueDomain();
        domain.setDatatype("DOUBLE");
        domain.setDescription("[0-9]*f");
        domain.setUnitOfMeasure("fucks");
        domain.setFormat("no format asshole");
        domain.setValidationType(ValidationType.NONE);

        edao.saveElement(domain);

        DataElement element = new DataElement();
        element.setValueDomainId(domain.getId());

        edao.saveElement(element);

        Element namespace = edao.getNamespaces().get(0);

        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setStatus(Status.RELEASED);
        identifier.setNamespaceId(namespace.getId());
        identifier.setUrl("http://this.does.not.exists");
        identifier.setVersion("1.2e");
        identifier.setElementId(element.getId());
        identifier.setIdentifier("" + element.getId());
        identifier.setElementType(ElementType.DATAELEMENT);
        identifier.setNamespace("mdr-test");
        identifier.setCreatedBy(1);

        dao.saveScopedIdentifier(identifier);

        assertTrue(dao.getScopedIdentifier(identifier.getId()).equals(identifier));
        DataElement e2 = (DataElement) edao.getElement(element.getId());
        assertTrue(e2.equals(element));

        commit(mdr);

        assertTrue(mdr.get(ScopedIdentifierDAO.class).findIdenticalScopedIdentifiers(identifier.toString(), 0).size() == 1);
        assertTrue(mdr.get(ScopedIdentifierDAO.class).findIdenticalScopedIdentifiers(identifier.toString(), 0).contains(identifier));
        assertTrue(mdr.get(ScopedIdentifierDAO.class).getIdenticalScopedIdentifier(identifier.toString(), identifier.getNamespace(), 0).equals(identifier));

        commit(mdr);
        mdr.close();
    }

}
