/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.sdao.DAOException;

public class DataElementGroupDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        MDRConnection mdr = MDRTestSuite.get();
        List<DataElement> elements = new ArrayList<DataElement>();
        ElementDAO dao = mdr.get(ElementDAO.class);

        DescribedValueDomain ds = new DescribedValueDomain();
        ds.setDescription("none");
        ds.setDatatype("FLOAT");
        ds.setFormat("fff.fff");
        ds.setMaxCharacters(4);
        ds.setUnitOfMeasure("Lightyears");
        ds.setValidationType(ValidationType.NONE);

        dao.saveElement(ds);
//        assertTrue(dsdao.getDescribedValueDomain(ds.getId()).equals(ds));

        DataElement element1 = new DataElement();
        element1.setValueDomainId(ds.getId());
        elements.add(element1);

        dao.saveElement(element1);
        assertTrue(dao.getElement(element1.getId()).equals(element1));

        DataElement element2 = new DataElement();
        element2.setValueDomainId(ds.getId());
        elements.add(element2);

        dao.saveElement(element2);
        assertTrue(dao.getElement(element2.getId()).equals(element2));

        DataElementGroup group = new DataElementGroup();

        dao.saveElement(group);
        assertTrue(dao.getElement(group.getId()).equals(group));

        commit(mdr);
        mdr.close();
    }

}
