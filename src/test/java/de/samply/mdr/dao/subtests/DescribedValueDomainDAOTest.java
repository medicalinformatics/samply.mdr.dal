/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.sdao.DAOException;

public class DescribedValueDomainDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        DescribedValueDomain domain = new DescribedValueDomain();
        domain.setDatatype("integer");
        domain.setDescription("[0-9]+");
        domain.setMaxCharacters(1000);
        domain.setUnitOfMeasure("years");
        domain.setFormat("none");
        domain.setValidationType(ValidationType.NONE);

        MDRConnection mdr = MDRTestSuite.get();
        DefinitionDAO ddao = mdr.get(DefinitionDAO.class);
        ElementDAO dao = mdr.get(ElementDAO.class);
        ScopedIdentifierDAO scdao = mdr.get(ScopedIdentifierDAO.class);

        dao.saveElement(domain);

        DescribedValueDomain describedValue = (DescribedValueDomain) dao.getElement(domain.getId());

        assertTrue(describedValue.equals(domain));

        DataElement element = new DataElement();
        element.setValueDomainId(describedValue.getId());

        dao.saveElement(element);

        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setStatus(Status.RELEASED);
        identifier.setUrl("http://NONE");
        identifier.setVersion("1.0");
        identifier.setNamespaceId(getNamespace(mdr).getId());
        identifier.setElementId(element.getId());
        identifier.setIdentifier("" + element.getId());
        identifier.setElementType(ElementType.DATAELEMENT);
        identifier.setCreatedBy(1);

        scdao.saveScopedIdentifier(identifier);

        Definition d = new Definition();
        d.setElementId(describedValue.getId());
        d.setDefinition("Eine ganze Zahl");
        d.setDesignation("none");
        d.setLanguage("de");
        d.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(d);
        assertTrue(ddao.getDefinition(d.getId()).equals(d));

        d = new Definition();
        d.setElementId(describedValue.getId());
        d.setDesignation("none");
        d.setDefinition("An integer");
        d.setLanguage("en");
        d.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(d);

        assertTrue(ddao.getDefinition(d.getId()).equals(d));
        assertTrue(ddao.getDefinitions(describedValue.getId(), identifier.getId()).size() == 2);

        d = new Definition();
        d.setElementId(element.getId());
        d.setDesignation("Patients age");
        d.setDefinition("The patients age at time of death in years");
        d.setLanguage("en");
        d.setScopedIdentifierId(identifier.getId());
        ddao.saveDefinition(d);

        d = new Definition();
        d.setElementId(element.getId());
        d.setDesignation("Alter des Patienten");
        d.setDefinition("Das Alter des Patienten zum Zeitpunkt des Todes");
        d.setLanguage("de");
        d.setScopedIdentifierId(identifier.getId());
        ddao.saveDefinition(d);

        commit(mdr);
        mdr.close();
    }

}
