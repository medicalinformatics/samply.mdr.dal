--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Contact: info@osse-register.de
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

CREATE TYPE "elementType" AS ENUM ('DATAELEMENTGROUP', 'RECORD', 'DATAELEMENT', 'CATALOG', 'CODE');

CREATE TYPE "validationType" AS ENUM ('NONE', 'BOOLEAN', 'INTEGER', 'FLOAT', 'INTEGERRANGE', 'FLOATRANGE', 'DATE',
            'DATETIME', 'TIME', 'REGEX', 'JS', 'LUA');

CREATE TYPE "status" AS ENUM ('DRAFT', 'RELEASED', 'OUTDATED');

CREATE TABLE "mdrUser" (id SERIAL PRIMARY KEY,
            "username" TEXT NOT NULL UNIQUE,
            "canCreateNamespace" BOOLEAN NOT NULL,
            "canCreateCatalog" BOOLEAN NOT NULL,
            "data" JSON NOT NULL);


CREATE TABLE "identified" ("id" SERIAL PRIMARY KEY);


CREATE TABLE "designatable" ("id" SERIAL PRIMARY KEY);


CREATE TABLE "valueMeaning" ("id" INTEGER NOT NULL PRIMARY KEY REFERENCES "designatable" ("id"));
CREATE INDEX ON "valueMeaning" ("id");

CREATE TABLE "valueDomain" ("id" SERIAL PRIMARY KEY,
            "datatype" TEXT NOT NULL,
            "maximumCharacters" INTEGER NOT NULL,
            "format" TEXT NOT NULL,
            "unitOfMeasure" VARCHAR(50));


CREATE TABLE "source" ("id" SERIAL PRIMARY KEY,
            "name" TEXT NOT NULL UNIQUE,
            "prefix" TEXT NOT NULL UNIQUE,
            "baseURL" TEXT NOT NULL UNIQUE);


CREATE TABLE "namespace" ("id" SERIAL PRIMARY KEY,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id"),
            "sourceId" INTEGER NOT NULL REFERENCES "source" ("id"),
            "name" TEXT NOT NULL UNIQUE,
            "hidden" BOOLEAN NOT NULL DEFAULT FALSE,
            "createdBy" INTEGER NOT NULL REFERENCES "mdrUser" ("id"),
            "data" JSON NOT NULL);
CREATE INDEX ON "namespace" ("sourceId");
CREATE INDEX ON "namespace" ("designatableId");
CREATE INDEX ON "namespace" ("createdBy");


CREATE TABLE "scopedIdentifier" ("id" SERIAL PRIMARY KEY,
            "namespaceId" INTEGER NOT NULL REFERENCES "namespace" ("id"),
            "identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id"),
            "elementType" "elementType" NOT NULL,
            "version" VARCHAR(30) NOT NULL,
            "identifier" TEXT NOT NULL,
            "url" TEXT NOT NULL,
            "createdBy" INTEGER NOT NULL REFERENCES "mdrUser" ("id"),
            "uuid" SERIAL NOT NULL,
            "status" "status" NOT NULL,
            UNIQUE("identifier", "version", "namespaceId", "elementType"));
CREATE INDEX ON "scopedIdentifier" ("namespaceId");
CREATE INDEX ON "scopedIdentifier" ("identifiedId");
CREATE INDEX ON "scopedIdentifier" ("createdBy");
CREATE INDEX ON "scopedIdentifier" ("version");
CREATE INDEX ON "scopedIdentifier" ("identifier");
CREATE INDEX ON "scopedIdentifier" ("uuid");
CREATE INDEX ON "scopedIdentifier" ("status");


CREATE TABLE "definition" ("id" SERIAL PRIMARY KEY,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id"),
            "scopedIdentifierId" INTEGER REFERENCES "scopedIdentifier" ("id"),
            "designation" TEXT NOT NULL,
            "definition" TEXT NOT NULL,
            "language" VARCHAR(20) NOT NULL);
CREATE INDEX ON "definition" ("designatableId");
CREATE INDEX ON "definition" ("scopedIdentifierId");


CREATE TABLE "scopedIdentifierHierarchy" ("superId" INTEGER NOT NULL REFERENCES "scopedIdentifier" ("id"),
            "subId" INTEGER NOT NULL REFERENCES "scopedIdentifier" ("id"),
            "order" INTEGER NOT NULL DEFAULT 1,
            PRIMARY KEY("superId", "subId"));
CREATE INDEX ON "scopedIdentifierHierarchy" ("superId");
CREATE INDEX ON "scopedIdentifierHierarchy" ("subId");


CREATE TABLE "record" ("identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id") UNIQUE,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id") UNIQUE,
            PRIMARY KEY ("identifiedId", "designatableId"));
CREATE INDEX ON "record" ("identifiedId");
CREATE INDEX ON "record" ("designatableId");


CREATE TABLE "slot" ("id" SERIAL PRIMARY KEY,
            "scopedIdentifierId" INTEGER NOT NULL REFERENCES "scopedIdentifier" ("id"),
            "key" VARCHAR(100) NOT NULL,
            "value" TEXT NOT NULL);
CREATE INDEX ON "slot" ("scopedIdentifierId");


CREATE TABLE "dataElement" ("identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id") UNIQUE,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id") UNIQUE,
            "valueDomainId" INTEGER NOT NULL REFERENCES "valueDomain" ("id"),
            PRIMARY KEY("identifiedId", "designatableId"));
CREATE INDEX ON "dataElement" ("identifiedId");
CREATE INDEX ON "dataElement" ("designatableId");
CREATE INDEX ON "dataElement" ("valueDomainId");


CREATE TABLE "dataElementGroup" ("identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id") UNIQUE,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id") UNIQUE,
            PRIMARY KEY ("identifiedId", "designatableId"));
CREATE INDEX ON "dataElementGroup" ("identifiedId");
CREATE INDEX ON "dataElementGroup" ("designatableId");


CREATE TABLE "catalog" ("identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id") UNIQUE,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id") UNIQUE,
            PRIMARY KEY("identifiedId", "designatableId"));
CREATE INDEX ON "catalog" ("identifiedId");
CREATE INDEX ON "catalog" ("designatableId");


CREATE TABLE "code" ("identifiedId" INTEGER NOT NULL REFERENCES "identified" ("id") UNIQUE,
            "designatableId" INTEGER NOT NULL REFERENCES "designatable" ("id") UNIQUE,
            "catalogId" INTEGER NOT NULL REFERENCES "catalog" ("identifiedId"),
            "code" TEXT NOT NULL,
            "isValid" BOOLEAN NOT NULL,
            PRIMARY KEY("identifiedId", "designatableId"),
            UNIQUE("code", "catalogId"));
CREATE INDEX ON "code" ("identifiedId");
CREATE INDEX ON "code" ("designatableId");
CREATE INDEX ON "code" ("catalogId");


CREATE TABLE "enumeratedValueDomain" ("id" INTEGER NOT NULL PRIMARY KEY REFERENCES "valueDomain" ("id"));
CREATE INDEX ON "enumeratedValueDomain" ("id");


CREATE TABLE "permissibleValue" ("valueMeaningId" INTEGER NOT NULL REFERENCES "valueMeaning" ("id"),
            "enumeratedValueDomainId" INTEGER NOT NULL REFERENCES "enumeratedValueDomain" ("id"),
            "permittedValue" TEXT NOT NULL,
            PRIMARY KEY ("valueMeaningId", "enumeratedValueDomainId"));
CREATE INDEX ON "permissibleValue" ("valueMeaningId");
CREATE INDEX ON "permissibleValue" ("enumeratedValueDomainId");


CREATE TABLE "describedValueDomain" ("valueMeaningId" INTEGER NOT NULL REFERENCES "valueMeaning" ("id"),
            "valueDomainId" INTEGER NOT NULL REFERENCES "valueDomain" ("id"),
            "description" TEXT,
            "validationType" "validationType" NOT NULL,
            "validationData" TEXT,
            PRIMARY KEY("valueMeaningId", "valueDomainId"));
CREATE INDEX ON "describedValueDomain" ("valueMeaningId");
CREATE INDEX ON "describedValueDomain" ("valueDomainId");


INSERT INTO "source" ("name", "prefix", "baseURL") VALUES ('Samply MDR', 'mdr', 'http://mdr.samply.de');

CREATE TABLE "userReadableNamespace" ("userId" INTEGER NOT NULL REFERENCES "mdrUser" ("id"),
            "namespaceId" INTEGER NOT NULL REFERENCES "namespace" ("id"));
CREATE INDEX ON "userReadableNamespace" ("userId");
CREATE INDEX ON "userReadableNamespace" ("namespaceId");


CREATE TABLE "userWritableNamespace" ("userId" INTEGER NOT NULL REFERENCES "mdrUser" ("id"),
            "namespaceId" INTEGER NOT NULL REFERENCES "namespace" ("id"));
CREATE INDEX ON "userWritableNamespace" ("userId");
CREATE INDEX ON "userWritableNamespace" ("namespaceId");


CREATE TABLE "config" (id SERIAL PRIMARY KEY,
            "name" TEXT NOT NULL UNIQUE,
            "value" JSON NOT NULL);

CREATE TABLE "catalogValueDomain" ("id" INTEGER PRIMARY KEY REFERENCES "valueDomain" ("id"),
            "catalogScopedIdentifierId" INTEGER NOT NULL REFERENCES "scopedIdentifier" ("id"));
CREATE INDEX ON "catalogValueDomain" ("catalogScopedIdentifierId");






CREATE MATERIALIZED VIEW "hierarchy" AS (
    WITH RECURSIVE members(root, super, sub) AS
        (SELECT "superId", "superId", "subId" FROM "scopedIdentifierHierarchy"
            UNION ALL
         SELECT m."root", sc."superId", sc."subId" FROM members AS m INNER JOIN "scopedIdentifierHierarchy" AS sc ON m."sub" = sc."superId")
    SELECT m.root, m.super, m.sub FROM members AS m);

CREATE INDEX ON "hierarchy" ("super", "sub");
CREATE INDEX ON "hierarchy" ("sub");
CREATE INDEX ON "hierarchy" ("super");
CREATE INDEX ON "hierarchy" ("root");






CREATE TABLE "permissibleCode" ("id" SERIAL PRIMARY KEY,
            "catalogValueDomainId" INTEGER NOT NULL REFERENCES "catalogValueDomain" ("id"),
            "codeScopedIdentifierId" INTEGER NOT NULL REFERENCES "scopedIdentifier" ("id"));
CREATE INDEX ON "permissibleCode" ("catalogValueDomainId");
CREATE INDEX ON "permissibleCode" ("codeScopedIdentifierId");



INSERT INTO "config" ("name", "value") VALUES ('defaultConfig', '{"dbVersion":6, "defaultGrant":false, "defaultCatalogGrant":false}');
INSERT INTO "config" ("name", "value") VALUES ('config', '{}');


