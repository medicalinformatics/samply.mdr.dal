--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Contact: info@osse-register.de
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--



CREATE TABLE "element" ("id" SERIAL PRIMARY KEY,

        /**Ephemeral IDs, columns dropped after update */
        "designatableId" INTEGER REFERENCES "designatable" ("id"),
        "identifiedId" INTEGER REFERENCES "identified" ("id"),
        "valueDomainId2" INTEGER REFERENCES "valueDomain" ("id"),
        "valueDomainId" INTEGER REFERENCES "valueDomain" ("id"),
        "catalogId" INTEGER REFERENCES "element" ("id"),
        "namespaceId" INTEGER REFERENCES "namespace" ("id"),

        "elementType" "elementType" NOT NULL,

        "name" TEXT,
        "hidden" BOOLEAN,
        "createdBy" INTEGER REFERENCES "mdrUser" ("id"),

        /**
         * References to other elements (code, permissible values and data elements) and
         * scoped identifiers (catalog value domain references a scoped identifier from a catalog)
         */
        "elementId" INTEGER REFERENCES "element" ("id"),
        "scopedIdentifierId" INTEGER REFERENCES "scopedIdentifier" ("id"),

        "code"  TEXT,
        "isValid" BOOLEAN,

        "format" TEXT,
        "datatype" TEXT,
        "unitOfMeasure" TEXT,
        "maximumCharacters" INTEGER,

        "description" TEXT,
        "validationType" "validationType",
        "validationData" TEXT,

        "permittedValue" TEXT,

        "data" JSON DEFAULT '{}');

CREATE OR REPLACE FUNCTION getelementtype(id integer) RETURNS "elementType" AS $$ SELECT "elementType" from "element" WHERE "id" = $1 $$ LANGUAGE SQL;

INSERT INTO "element" ("designatableId", "identifiedId", "elementType", "valueDomainId2")
    (SELECT "designatableId", "identifiedId", 'DATAELEMENT'::"elementType", "valueDomainId" FROM "dataElement");

INSERT INTO "element" ("designatableId", "identifiedId", "elementType")
    (SELECT "designatableId", "identifiedId", 'DATAELEMENTGROUP'::"elementType" FROM "dataElementGroup");

INSERT INTO "element" ("designatableId", "identifiedId", "elementType")
    (SELECT "designatableId", "identifiedId", 'RECORD'::"elementType" FROM "record");

INSERT INTO "element" ("designatableId", "identifiedId", "elementType")
    (SELECT "designatableId", "identifiedId", 'CATALOG'::"elementType" FROM "catalog");

INSERT INTO "element" ("designatableId", "identifiedId", "elementType", "catalogId", "code", "isValid")
    (SELECT "designatableId", "identifiedId", 'CODE'::"elementType", "catalogId", "code", "isValid" FROM "code");

INSERT INTO "element" ("designatableId", "identifiedId", "elementType", "name", "hidden", "createdBy", "data", "namespaceId")
    (SELECT "designatableId", NULL, 'NAMESPACE'::"elementType", "name", "hidden", "createdBy", "data", "id" FROM "namespace");

INSERT INTO "element" ("designatableId", "identifiedId", "valueDomainId", "elementType", "description", "validationType",
    "validationData", "format", "datatype", "unitOfMeasure", "maximumCharacters")
    (SELECT d."valueMeaningId", NULL, v.id, 'DESCRIBED_VALUE_DOMAIN'::"elementType", d."description", d."validationType",
    d."validationData", v."format", v."datatype", v."unitOfMeasure", v."maximumCharacters" FROM "describedValueDomain" AS d LEFT JOIN
    "valueDomain" AS v ON v.id = d."valueDomainId");

INSERT INTO "element" ("designatableId", "identifiedId", "valueDomainId", "elementType", "format", "datatype", "unitOfMeasure", "maximumCharacters")
    (SELECT NULL, NULL, v.id, 'ENUMERATED_VALUE_DOMAIN'::"elementType", v."format", v."datatype", v."unitOfMeasure", v."maximumCharacters" FROM
    "enumeratedValueDomain" AS e LEFT JOIN "valueDomain" AS v ON v.id = e."id");

INSERT INTO "element" ("designatableId", "identifiedId", "valueDomainId", "elementType", "scopedIdentifierId", "format", "datatype", "unitOfMeasure", "maximumCharacters")
    (SELECT NULL, NULL, v.id, 'CATALOG_VALUE_DOMAIN'::"elementType", c."catalogScopedIdentifierId", v."format", v."datatype", v."unitOfMeasure", v."maximumCharacters" FROM
    "catalogValueDomain" AS c LEFT JOIN "valueDomain" AS v ON v.id = c."id");

INSERT INTO "element" ("designatableId", "identifiedId", "valueDomainId2", "elementType", "permittedValue")
    (SELECT pv."valueMeaningId", NULL, pv."enumeratedValueDomainId", 'PERMISSIBLE_VALUE'::"elementType", pv."permittedValue" FROM
    "permissibleValue" AS pv);


UPDATE "element" AS e SET "elementId" = e2.id FROM element AS e2 WHERE e2."valueDomainId" = e."valueDomainId2";
UPDATE "element" AS e SET "elementId" = e2.id FROM element AS e2 WHERE e."catalogId" = e2."identifiedId";

ALTER TABLE "permissibleCode" ADD COLUMN "catalogId" INTEGER REFERENCES "element" ("id");
UPDATE "permissibleCode" AS p SET "catalogId" = e.id FROM element AS e WHERE e."valueDomainId" = p."catalogValueDomainId";
ALTER TABLE "permissibleCode" ALTER COLUMN "catalogId" SET NOT NULL;
CREATE INDEX ON "permissibleCode" ("catalogId");

ALTER TABLE "scopedIdentifier" ADD COLUMN "elementId" INTEGER REFERENCES "element" ("id");
ALTER TABLE "scopedIdentifier" ADD COLUMN "namespaceId2" INTEGER REFERENCES "element" ("id");

UPDATE "scopedIdentifier" AS sc SET "elementId" = e.id FROM element AS e WHERE e."identifiedId" = sc."identifiedId";
UPDATE "scopedIdentifier" AS sc SET "namespaceId2" = e.id FROM element AS e WHERE e."namespaceId" = sc."namespaceId";

ALTER TABLE "scopedIdentifier" DROP COLUMN "namespaceId";
ALTER TABLE "scopedIdentifier" RENAME COLUMN "namespaceId2" TO "namespaceId";
ALTER TABLE "scopedIdentifier" ALTER COLUMN "namespaceId" SET NOT NULL;

ALTER TABLE "scopedIdentifier" ALTER COLUMN "elementId" SET NOT NULL;
ALTER TABLE "scopedIdentifier" DROP COLUMN "identifiedId";

ALTER TABLE "definition" ADD COLUMN "elementId" INTEGER REFERENCES "element" ("id");

UPDATE "definition" AS d SET "elementId" = e.id FROM element AS e WHERE e."designatableId" = d."designatableId";

ALTER TABLE "definition" ALTER COLUMN "elementId" SET NOT NULL;
ALTER TABLE "definition" DROP COLUMN "designatableId";
CREATE INDEX ON "definition" ("elementId");

ALTER TABLE "userReadableNamespace" ADD COLUMN "namespaceId2" INTEGER REFERENCES "element" ("id");
ALTER TABLE "userWritableNamespace" ADD COLUMN "namespaceId2" INTEGER REFERENCES "element" ("id");

UPDATE "userReadableNamespace" AS ur SET "namespaceId2" = e.id FROM element AS e WHERE e."namespaceId" = ur."namespaceId";
UPDATE "userWritableNamespace" AS ur SET "namespaceId2" = e.id FROM element AS e WHERE e."namespaceId" = ur."namespaceId";

ALTER TABLE "userReadableNamespace" DROP COLUMN "namespaceId";
ALTER TABLE "userWritableNamespace" DROP COLUMN "namespaceId";

ALTER TABLE "userReadableNamespace" RENAME COLUMN "namespaceId2" TO "namespaceId";
ALTER TABLE "userReadableNamespace" ALTER COLUMN "namespaceId" SET NOT NULL;
ALTER TABLE "userWritableNamespace" RENAME COLUMN "namespaceId2" TO "namespaceId";
ALTER TABLE "userWritableNamespace" ALTER COLUMN "namespaceId" SET NOT NULL;

CREATE INDEX ON "userReadableNamespace" ("namespaceId");
CREATE INDEX ON "userWritableNamespace" ("namespaceId");


DROP TABLE "describedValueDomain" CASCADE;
DROP TABLE "enumeratedValueDomain" CASCADE;
DROP TABLE "valueDomain" CASCADE;
DROP TABLE "dataElement" CASCADE;
DROP TABLE "catalog" CASCADE;
DROP TABLE "valueMeaning" CASCADE;
DROP TABLE "dataElementGroup" CASCADE;
DROP TABLE "record" CASCADE;
DROP TABLE "catalogValueDomain" CASCADE;
DROP TABLE "code" CASCADE;
DROP TABLE "identified" CASCADE;
DROP TABLE "designatable" CASCADE;
DROP TABLE "permissibleValue" CASCADE;
DROP TABLE "namespace" CASCADE;

ALTER TABLE element DROP COLUMN "designatableId";
ALTER TABLE element DROP COLUMN "identifiedId";
ALTER TABLE element DROP COLUMN "valueDomainId2";
ALTER TABLE element DROP COLUMN "valueDomainId";
ALTER TABLE element DROP COLUMN "catalogId";
ALTER TABLE element DROP COLUMN "namespaceId";

ALTER TABLE "permissibleCode" DROP COLUMN "id";
ALTER TABLE "permissibleCode" DROP COLUMN "catalogValueDomainId";
ALTER TABLE "permissibleCode" RENAME COLUMN "catalogId" TO "catalogValueDomainId";

CREATE INDEX ON "element" ("id", "elementType");
CREATE INDEX ON "element" ("createdBy");
CREATE INDEX ON "element" ("elementId");
CREATE INDEX ON "element" ("elementType");


/**
 * Some checks for elements
 */

ALTER TABLE "element" ADD CONSTRAINT de_check CHECK ("elementType" != 'DATAELEMENT' OR ("elementId" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT enu_check CHECK ("elementType" != 'ENUMERATED_VALUE_DOMAIN' OR
    ("format" IS NOT NULL AND "datatype" IS NOT NULL AND "maximumCharacters" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT desc_check CHECK ("elementType" != 'DESCRIBED_VALUE_DOMAIN' OR
    ("format" IS NOT NULL AND "datatype" IS NOT NULL AND "maximumCharacters" IS NOT NULL
    AND "validationType" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT catalog_check CHECK ("elementType" != 'CATALOG_VALUE_DOMAIN' OR
    ("format" IS NOT NULL AND "datatype" IS NOT NULL AND "maximumCharacters" IS NOT NULL
    AND "scopedIdentifierId" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT ns_check CHECK ("elementType" != 'NAMESPACE' OR
    ("hidden" IS NOT NULL AND "name" IS NOT NULL AND "createdBy" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT code_check CHECK ("elementType" != 'CODE' OR
    ("code" IS NOT NULL AND "isValid" IS NOT NULL AND "elementId" IS NOT NULL));

ALTER TABLE "element" ADD CONSTRAINT pv_check CHECK ("elementType" != 'PERMISSIBLE_VALUE' OR
    ("permittedValue" IS NOT NULL AND "elementId" IS NOT NULL));

ALTER TABLE "scopedIdentifier" ADD CONSTRAINT et_check CHECK ("elementType" = getelementtype("elementId"));
