/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.mdr.dal.dto.User;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 *The DAO for users.
 */
public class UserDAO extends AbstractDAO<User>{

    protected UserDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Returns the user with the given identity from OAuth2 (OpenID-Connect).
     * @param identity
     * @return
     * @throws DAOException
     */
    public User getUserByIdentity(String identity) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.USERNAME.access() + " = ?", identity);
    }

    /**
     * Returns the user with the given email.
     * @param email
     * @return
     * @throws DAOException
     */
    @Deprecated
    public User getUserByEmail(String email) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.DATA.access() + "->>'email' = ?", email);
    }

    /**
     * Returns the user with the given ID.
     * @param userId
     * @return
     * @throws DAOException
     */
    public User getUser(int userId) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + User.ID.access() + " = ?", userId);
    }

    /**
     * Returns all users.
     * @return
     * @throws DAOException
     */
    public List<User> getUsers() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE);
    }

    /**
     * Updated the users data.
     * @param user
     * @throws DAOException
     */
    public void updateUser(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()), User.DATA.val(user.getData()));
    }

    /**
     * Updated the users permissions.
     * @param user
     * @throws DAOException
     */
    public void updatePermissions(User user) throws DAOException {
        update(User.TABLE, User.ID.val(user.getId()),
                User.CREATE_CATALOG.val(user.getCanCreateCatalog()),
                User.CREATE_NAMESPACE.val(user.getCanCreateNamespace()),
                User.EXPORT_IMPORT.val(user.getCanExportImport()));
    }

    /**
     * Save the given user.
     * @param user
     * @return
     * @throws DAOException
     */
    private User saveUser(User user) throws DAOException {
        user.setId(insert(User.TABLE,
                User.USERNAME.val(user.getUsername()),
                User.CREATE_NAMESPACE.val(user.getCanCreateNamespace()),
                User.CREATE_CATALOG.val(user.getCanCreateCatalog()),
                User.EXPORT_IMPORT.val(user.getCanExportImport()),
                User.DATA.val(user.getData())));
        return user;
    }

    /**
     * Creates a new user with the given attributes. Also saves the user.
     * @param identity
     * @param email
     * @param realName
     * @param canCreateNamespaces
     * @param canCreateCatalogs
     * @return
     * @throws DAOException
     */
    public User createDefaultUser(String identity, String email, String realName, String externalLabel,
            boolean canCreateNamespaces, boolean canCreateCatalogs, boolean canExportImport) throws DAOException {
        User user = new User();
        user.setCanCreateNamespace(canCreateNamespaces);
        user.setCanCreateCatalog(canCreateCatalogs);
        user.setCanExportImport(canExportImport);
        user.setUsername(identity);
        user.setEmail(email);
        user.setRealName(realName);
        user.setExternalLabel(externalLabel);
        return saveUser(user);
    }


    @Override
    public User getObject(ResultSet set) throws SQLException {
        User user = new User();
        user.setId(set.getInt(User.ID.alias));
        user.setUsername(set.getString(User.USERNAME.alias));
        user.setCanCreateNamespace(set.getBoolean(User.CREATE_NAMESPACE.alias));
        user.setCanCreateCatalog(set.getBoolean(User.CREATE_CATALOG.alias));
        user.setCanExportImport(set.getBoolean(User.EXPORT_IMPORT.alias));
        user.setData(asJson(set.getString(User.DATA.alias)));
        return user;
    }

    @Override
    protected String getInsertTable() {
        throw new UnsupportedOperationException();
    }

    public static final String SQL_TABLE = User.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(User.class);

}
