/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

/**
 * A static class that defines the properties in various JSON objects.
 * @author paul
 *
 */
public class Vocabulary {

    public static final String EMAIL = "email";
    public static final String DB_VERSION = "dbVersion";
    public static final String DEFAULT_NAMESPACE_GRANT = "defaultNamespaceGrant";
    public static final String DEFAULT_CATALOG_GRANT = "defaultCatalogGrant";
    public static final String DEFAULT_EXPORT_IMPORT_GRANT = "defaultExportImportGrant";
    public static final String SHOW_TRANSLATION_BUTTON = "showTranslationButton";

    public static final String REALNAME = "realName";
    public static final String EXTERNAL_LABEL = "externalLabel";

    public static final String LANGUAGES = "languages";
    public static final String SLOTS = "slots";
    public static final String CONSTRAINTS = "constraints";

}
