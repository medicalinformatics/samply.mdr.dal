/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.samply.mdr.dal.dto.Misc;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * DAO for slots.
 */
public class SlotDAO extends AbstractDAO<Slot> {

    protected SlotDAO(ConnectionProvider master) {
        super(master);
    }

    @Override
    public Slot getObject(ResultSet set) throws SQLException {
        Slot s = new Slot();
        s.setId(set.getInt(Slot.ID.alias));
        s.setScopedIdentifierId(set.getInt(Slot.SCOPED_ID.alias));
        s.setKey(set.getString(Slot.KEY.alias));
        s.setValue(set.getString(Slot.VALUE.alias));
        return s;
    }

    /**
     * Returns the slots for the given URN.
     * @param urn
     * @return
     * @throws DAOException
     */
    public List<Slot> getSlots(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " "
                + "LEFT JOIN " + ScopedIdentifier.TABLE.from() + " ON " + Slot.SCOPED_ID.access() + " = " + ScopedIdentifier.ID.access()
                + " LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE "
                + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                + ScopedIdentifier.VERSION.access() + " = ? AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                + Misc.NAME.access() + " = ?",
                    identifier.getIdentifier(), identifier.getVersion(), identifier.getElementType(), identifier.getNamespace());
    }

    /**
     * Saves the given slots.
     * @param slot
     * @throws DAOException
     */
    public void saveSlot(Slot slot) throws DAOException {
        insert(Slot.TABLE,
                Slot.SCOPED_ID.val(slot.getScopedIdentifierId()),
                Slot.KEY.val(slot.getKey()),
                Slot.VALUE.val(slot.getValue()));
    }

    static final String SQL_SELECT_FIELDS = getSelectFields(Slot.class);

    static final String SQL_TABLE = Slot.TABLE.from();

    @Override
    protected String getInsertTable() {
        throw new UnsupportedOperationException();
    }
}
