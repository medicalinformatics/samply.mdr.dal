/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import de.samply.mdr.dal.dto.Catalog;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.Misc.UserReadableNamespace;
import de.samply.mdr.dal.dto.Misc.UserWritableNamespace;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 * The DAO that can be used to get all sorts of elements and especially namespaces.
 */
public class ElementDAO extends AbstractDAO<Element> {

    protected ElementDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Saves the given element.
     * @param element
     * @throws DAOException
     */
    public void saveElement(Element element) throws DAOException {
        if(element.getUuid() == null) {
            element.setUuid(UUID.randomUUID());
        }

        if(element instanceof DataElement) {
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.DATAELEMENT),
                    Element.ELEMENT_ID.val(((DataElement) element).getValueDomainId()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.DATAELEMENT);

        } else if(element instanceof DataElementGroup) {
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.DATAELEMENTGROUP),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.DATAELEMENTGROUP);

        } else if(element instanceof Record) {
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.RECORD),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.RECORD);

        } else if(element instanceof Catalog) {
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.CATALOG),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.CATALOG);

        } else if(element instanceof Code) {
            Code c = (Code) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.CODE),
                    Element.ELEMENT_ID.val(c.getCatalogId()),
                    Code.CODE.val(c.getCode()),
                    Code.IS_VALID.val(c.isValid()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.CODE);

        } else if(element instanceof Namespace) {
            Namespace ns = (Namespace) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.NAMESPACE),
                    Namespace.NAME.val(ns.getName()),
                    Namespace.HIDDEN.val(ns.getHidden()),
                    Namespace.CREATED_BY.val(ns.getCreatedBy()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.NAMESPACE);

            insert(UserReadableNamespace.TABLE,
                    UserReadableNamespace.NAMESPACE_ID.val(ns.getId()),
                    UserReadableNamespace.USER_ID.val(getUserId()));

            insert(UserWritableNamespace.TABLE,
                    UserWritableNamespace.NAMESPACE_ID.val(ns.getId()),
                    UserWritableNamespace.USER_ID.val(getUserId()));

        } else if(element instanceof EnumeratedValueDomain) {
            EnumeratedValueDomain evd = (EnumeratedValueDomain) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.ENUMERATED_VALUE_DOMAIN),
                    ValueDomain.FORMAT.val(evd.getFormat()),
                    ValueDomain.DATATYPE.val(evd.getDatatype()),
                    ValueDomain.MAX_CHARACTERS.val(evd.getMaxCharacters()),
                    ValueDomain.UNIT_OF_MEASURE.val(evd.getUnitOfMeasure()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.ENUMERATED_VALUE_DOMAIN);

        } else if(element instanceof DescribedValueDomain) {
            DescribedValueDomain dvd = (DescribedValueDomain) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.DESCRIBED_VALUE_DOMAIN),
                    ValueDomain.FORMAT.val(dvd.getFormat()),
                    ValueDomain.DATATYPE.val(dvd.getDatatype()),
                    ValueDomain.MAX_CHARACTERS.val(dvd.getMaxCharacters()),
                    ValueDomain.UNIT_OF_MEASURE.val(dvd.getUnitOfMeasure()),
                    DescribedValueDomain.DESCRIPTION.val(dvd.getDescription()),
                    DescribedValueDomain.VALIDATION_DATA.val(dvd.getValidationData()),
                    DescribedValueDomain.VALIDATION_TYPE.val(dvd.getValidationType()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.DESCRIBED_VALUE_DOMAIN);

        } else if(element instanceof CatalogValueDomain) {
            CatalogValueDomain cvd = (CatalogValueDomain) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.CATALOG_VALUE_DOMAIN),
                    ValueDomain.FORMAT.val(cvd.getFormat()),
                    ValueDomain.DATATYPE.val(cvd.getDatatype()),
                    ValueDomain.MAX_CHARACTERS.val(cvd.getMaxCharacters()),
                    ValueDomain.UNIT_OF_MEASURE.val(cvd.getUnitOfMeasure()),
                    Element.SCOPED_ID.val(cvd.getCatalogScopedIdentifierId()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.CATALOG_VALUE_DOMAIN);

        } else if(element instanceof PermissibleValue) {
            PermissibleValue pv = (PermissibleValue) element;
            element.setId(insert(Element.TABLE,
                    Element.ELEMENT_TYPE.val(ElementType.PERMISSIBLE_VALUE),
                    PermissibleValue.PERMITTED_VALUE.val(pv.getPermittedValue()),
                    Element.ELEMENT_ID.val(pv.getValueDomainId()),
                    Element.UUID.val(element.getUuid())));
            element.setElementType(ElementType.PERMISSIBLE_VALUE);

        } else {
            throw new UnsupportedOperationException();
        }

        if(element.getExternalId() != null) {
            update(Element.TABLE, Element.ID.val(element.getId()),
                    Element.EXTERNAL_ID.val(element.getExternalId()));
        }
    }

    /**
     * Returns a list of *all* namespaces.
     * @return
     * @throws DAOException
     */
    public List<Element> getNamespaces() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE'");
    }

    /**
     * Returns the namespace with the given name..
     * @param name
     * @return
     * @throws DAOException
     */
    public Namespace getNamespace(String name) throws DAOException {
        return (Namespace) executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Namespace.NAME.access() + " = ? AND " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE'", name);
    }

    /**
     * Returns the element with the given id.
     * @param id
     * @return
     * @throws DAOException
     */
    public Element getElement(int id) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Element.ID.access() + " = ?", id);
    }

    /**
     * Returns the element with the given uuid.
     * @param uuid
     * @return
     * @throws DAOException
     */
    public Element getElement(UUID uuid) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Element.UUID.access() + " = ?::uuid", uuid);
    }

    /**
     * Returns all permissible values for the given value domain ID.
     * @param id
     * @return
     * @throws DAOException
     */
    public List<Element> getPermissibleValues(int id) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Element.ELEMENT_TYPE.access() + " = 'PERMISSIBLE_VALUE' AND "
                + Element.ELEMENT_ID.access() + " = ?", id);
    }

    @Override
    public Element getObject(ResultSet set) throws SQLException {
        ElementType type = ElementType.valueOf(set.getString(Element.ELEMENT_TYPE.alias));

        Element element = null;

        switch(type) {
            case DATAELEMENT:
                DataElement de = new DataElement();
                de.setValueDomainId(set.getInt(Element.ELEMENT_ID.alias));
                element = de;
                break;

            case DATAELEMENTGROUP:
                element = new DataElementGroup();
                break;

            case RECORD:
                element = new Record();
                break;

            case NAMESPACE:
                Namespace ns = new Namespace();
                ns.setCreatedBy(set.getInt(Namespace.CREATED_BY.alias));
                ns.setData(asJson(set.getString(Namespace.DATA.alias)));
                ns.setHidden(set.getBoolean(Namespace.HIDDEN.alias));
                ns.setName(set.getString(Namespace.NAME.alias));
                element = ns;
                break;

            case CATALOG:
                element = new Catalog();
                break;

            case CODE:
                Code code = new Code();
                code.setCode(set.getString(Code.CODE.alias));
                code.setValid(set.getBoolean(Code.IS_VALID.alias));
                code.setCatalogId(set.getInt(Element.ELEMENT_ID.alias));
                element = code;
                break;

            case PERMISSIBLE_VALUE:
                PermissibleValue value = new PermissibleValue();
                value.setPermittedValue(set.getString(PermissibleValue.PERMITTED_VALUE.alias));
                value.setValueDomainId(set.getInt(Element.ELEMENT_ID.alias));
                element = value;
                break;

            case CATALOG_VALUE_DOMAIN:
                CatalogValueDomain cvd = new CatalogValueDomain();
                cvd.setCatalogScopedIdentifierId(set.getInt(Element.SCOPED_ID.alias));
                fillValueDomainFields(cvd, set);
                element = cvd;
                break;

            case DESCRIBED_VALUE_DOMAIN:
                DescribedValueDomain dvd = new DescribedValueDomain();
                dvd.setDescription(set.getString(DescribedValueDomain.DESCRIPTION.alias));
                dvd.setValidationData(set.getString(DescribedValueDomain.VALIDATION_DATA.alias));
                dvd.setValidationType(ValidationType.valueOf(set.getString(DescribedValueDomain.VALIDATION_TYPE.alias)));
                fillValueDomainFields(dvd, set);
                element = dvd;
                break;

            case ENUMERATED_VALUE_DOMAIN:
                EnumeratedValueDomain evd = new EnumeratedValueDomain();
                fillValueDomainFields(evd, set);
                element = evd;
                break;

            default:
                throw new UnsupportedOperationException();
        }

        element.setId(set.getInt(Element.ID.alias));
        element.setUuid(UUID.fromString(set.getString(Element.UUID.alias)));
        element.setElementType(type);
        element.setExternalId(set.getString(Element.EXTERNAL_ID.alias));

        return element;
    }

    /**
     * Used by the mapper to set all value domain fields.
     * @param vd
     * @param set
     * @throws SQLException
     */
    private void fillValueDomainFields(ValueDomain vd, ResultSet set) throws SQLException {
        vd.setDatatype(set.getString(ValueDomain.DATATYPE.alias));
        vd.setFormat(set.getString(ValueDomain.FORMAT.alias));
        vd.setMaxCharacters(set.getInt(ValueDomain.MAX_CHARACTERS.alias));
        vd.setUnitOfMeasure(set.getString(ValueDomain.UNIT_OF_MEASURE.alias));
    }

    public static final String SQL_TABLE = Element.TABLE.from();

    public static final String SQL_SELECT_FIELDS = getSelectFields(Element.class, Namespace.class, DataElement.class,
            DataElementGroup.class, Record.class, ValueDomain.class, DescribedValueDomain.class, EnumeratedValueDomain.class,
            CatalogValueDomain.class, PermissibleValue.class, Code.class);

    @Override
    protected String getInsertTable() {
        throw new UnsupportedOperationException();
    }
}
