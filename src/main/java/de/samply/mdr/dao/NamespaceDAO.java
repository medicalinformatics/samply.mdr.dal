/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.Misc.UserReadableNamespace;
import de.samply.mdr.dal.dto.Misc.UserWritableNamespace;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;

/**
 *
 */
public class NamespaceDAO extends AbstractDAO<DescribedElement> {

    /**
     * A hashmap index for described elements
     */
    private HashMap<Integer, DescribedElement> describedIndexElement;

    /**
     * @param provider
     */
    protected NamespaceDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Updates the given namespace with the given definitions.
     * @param ns
     * @param definitions
     * @throws DAOException
     */
    public void updateNamespace(Namespace ns, List<Definition> definitions) throws DAOException {
        delete(Definition.TABLE, Definition.ELEMENT_ID.val(ns.getId()));
        DefinitionDAO defDao = provider.get(DefinitionDAO.class);

        for(Definition definition : definitions) {
            definition.setElementId(ns.getId());
            defDao.saveNamespaceDefinition(definition);
        }
        updateNamespace(ns);
    }

    /**
     * Updates the visibility and data of the given namespace.
     * @param ns
     * @throws DAOException
     */
    public void updateNamespace(Namespace ns) throws DAOException {
        update(Element.TABLE, Element.ID.val(ns.getId()),
                Namespace.HIDDEN.val(ns.getHidden()),
                Namespace.DATA.val(ns.getData()));
    }

    /**
     * Returns a list of all namespaces.
     * @return
     * @throws DAOException
     */
    public List<DescribedElement> getNamespaces() throws DAOException {
        init();

        return executeSelect(SQL_SELECT + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE'");
    }

    /**
     * Returns the namespace with the given name.
     * @param name
     * @return
     * @throws DAOException
     */
    public DescribedElement getNamespace(String name) throws DAOException {
        init();

        return executeSingleSelect(SQL_SELECT + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE' AND "
                + Namespace.NAME.access() + " = ? AND " + SQL_ACCESS_CLAUSE,
                name, getUserId());
    }

    /**
     * Returns all readable namespaces, including the implicitly readable namespaces.
     * @return
     * @throws DAOException
     */
    public List<DescribedElement> getReadableNamespaces() throws DAOException {
        init();

        return executeSelect(SQL_SELECT + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE' AND "
                + SQL_ACCESS_CLAUSE,
                getUserId());
    }

    /**
     * Returns all readable namespaces (which are not hidden or which are readable for the user as defined in the "userReadableNamespace"
     * table.
     * @return
     * @throws DAOException
     */
    public List<DescribedElement> getWritableNamespaces() throws DAOException {
        init();

        return executeSelect(SQL_SELECT + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE' "
                + "AND " + Element.ID.access() + " IN "
                        + "(SELECT " + UserWritableNamespace.NAMESPACE_ID.access() + " FROM " + UserWritableNamespace.TABLE.from()
                        + " WHERE " + UserWritableNamespace.USER_ID.access() + " = ?)",
                getUserId());
    }

    /**
     * Returns a list of namespaces which the user can explicitly read (as defined in the "userReadableNamespace" table)
     * @return
     * @throws DAOException
     */
    public List<DescribedElement> getExplicitlyReadableNamespaces() throws DAOException {
        init();

        return executeSelect(SQL_SELECT + " WHERE " + Element.ELEMENT_TYPE.access() + " = 'NAMESPACE' "
                + "AND " + Element.ID.access() + " IN "
                        + "(SELECT " + UserReadableNamespace.NAMESPACE_ID.access() + " FROM " + UserReadableNamespace.TABLE.from() +
                        " WHERE " + UserReadableNamespace.USER_ID.access() + " = ?)",
                        getUserId());
    }

    /**
     * Returns a list of namespaces which match the given input text.
     * @param input
     * @return
     * @throws DAOException
     */
    public List<DescribedElement> findNamespaces(String input) throws DAOException {
        init();

        input = "%" + input + "%";
        return executeSelect(SQL_SELECT + " WHERE "
                + "(" + Definition.DEFINITION.access() + " ILIKE ? OR " + Definition.DESIGNATION.access() + " ILIKE ?) AND "
                + "(" + Namespace.HIDDEN.access() + " = false OR " + Element.ID.access()
                    + " IN (SELECT " + UserReadableNamespace.NAMESPACE_ID.access() + " FROM "
                    + UserReadableNamespace.TABLE.from() + " WHERE " + UserReadableNamespace.USER_ID.access() + " = ?))",
                    input, input, getUserId());
    }

    /* (non-Javadoc)
     * @see de.samply.sdao.AbstractDAO#getObject(java.sql.ResultSet)
     */
    @Override
    public DescribedElement getObject(ResultSet set) throws SQLException {
        if(describedIndexElement.containsKey(set.getInt(Element.ID.alias))) {
            describedIndexElement.get(set.getInt(Element.ID.alias)).getDefinitions().add(provider.get(DefinitionDAO.class).getObject(set));
            return null;
        } else {
            DescribedElement identified = new DescribedElement();
            identified.setElement(provider.get(ElementDAO.class).getObject(set));

            identified.setDefinitions(new ArrayList<Definition>());

            if(set.getInt(Definition.ID.alias) != 0) {
                identified.getDefinitions().add(provider.get(DefinitionDAO.class).getObject(set));
            }

            describedIndexElement.put(identified.getElement().getId(), identified);

            return identified;
        }
    }

    /**
     *
     */
    public void init() {
        describedIndexElement = new HashMap<>();
    }


    public static final String SQL_SELECT = "SELECT " + IdentifiedDAO.SQL_DESCRIBED_FIELDS + " FROM " + Element.TABLE.from()
            + " LEFT JOIN " + DefinitionDAO.SQL_TABLE
            + " ON " + Definition.ELEMENT_ID.access() + " = " + Element.ID.access();

    public static final String SQL_ACCESS_CLAUSE = "(" + Namespace.HIDDEN.access() + " = false OR "
            + Namespace.ID.access() + " IN (SELECT " + UserReadableNamespace.NAMESPACE_ID.access() + " FROM "
                + UserReadableNamespace.TABLE.from() + " WHERE " + UserReadableNamespace.USER_ID.access() + " = ?))";

    public static final String SQL_SELECT_HIERARCHY = "SELECT " + IdentifiedDAO.SQL_SELECT_FIELDS + " FROM " + HierarchyNode.TABLE.from() + " LEFT JOIN " +
            ScopedIdentifierDAO.SQL_TABLE + " ON " + HierarchyNode.SUB.access() + " = " + ScopedIdentifier.ID.access() + " " + IdentifiedDAO.SQL_JOINS;

}
