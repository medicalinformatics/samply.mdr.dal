/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.upgrade;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dao.Vocabulary;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.ObjectMapper;
import de.samply.sdao.UpgradeExecutor;
import de.samply.sdao.json.JSONResource;

public class Upgrade8to9 extends UpgradeExecutor {

    protected Upgrade8to9(ConnectionProvider provider) {
        super(provider, "/sql/upgrade/upgrade8to9_1.sql");
    }

    @Override
    public boolean executeUpgrade(boolean dryRun) throws DAOException {

        List<UpdateElement> elements = executeSelect("SELECT id FROM " + ScopedIdentifier.TABLE.table(), mapper);

        for(UpdateElement e : elements) {
            update(ScopedIdentifier.TABLE,
                    ScopedIdentifier.ID.val(e.id),
                    ScopedIdentifier.UUID.val(UUID.randomUUID()));
        }

        elements = executeSelect("SELECT id FROM " + Element.TABLE.table(), mapper);

        for(UpdateElement e : elements) {
            update(Element.TABLE,
                    Element.ID.val(e.id),
                    Element.UUID.val(UUID.randomUUID()));
        }

        Reader reader = new InputStreamReader(this.getClass().getResourceAsStream("/sql/upgrade/upgrade8to9_2.sql"),
                StandardCharsets.UTF_8);
        executeStream(reader);

        return true;
    }

    @Override
    public JSONResource getDefaultConfig() {
        JSONResource config = new JSONResource();
        config.addProperty(Vocabulary.DB_VERSION, 9);
        config.addProperty(Vocabulary.DEFAULT_NAMESPACE_GRANT, false);
        config.addProperty(Vocabulary.DEFAULT_CATALOG_GRANT, false);
        config.addProperty(Vocabulary.DEFAULT_EXPORT_IMPORT_GRANT, false);
        return config;
    }

    private static class UpdateElement {

        private int id;

    }

    private static ObjectMapper<UpdateElement> mapper = new ObjectMapper<Upgrade8to9.UpdateElement>() {
        @Override
        public UpdateElement getObject(ResultSet set) throws SQLException {
            UpdateElement element = new UpdateElement();
            element.id = set.getInt("id");
            return element;
        }
    };

}
