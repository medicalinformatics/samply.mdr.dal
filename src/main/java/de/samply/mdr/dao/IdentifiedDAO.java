/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Misc;
import de.samply.mdr.dal.dto.Misc.ScopedHierarchy;
import de.samply.mdr.dal.dto.Misc.UserReadableNamespace;
import de.samply.mdr.dal.dto.Misc.UserWritableNamespace;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * High Level DAO used to get identified elements from the MDR.
 * @author paul
 *
 */
public class IdentifiedDAO extends AbstractDAO<IdentifiedElement> {

    /**
     * A hashmap index for identified elements
     */
    private HashMap<Integer, IdentifiedElement> indexElement;

    protected IdentifiedDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Searches the database for elements with the specified text, slots and status.
     * @param input
     * @param selectedNamespace
     * @param selectedTypes
     * @param selectedStatuses
     * @param slots
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> findElements(String input, String selectedNamespace, ElementType[] selectedTypes,
            Status[] selectedStatuses, HashMap<String, String> slots) throws DAOException {
        init();

        List<Object> objs = new ArrayList<>();
        List<String> clauses = new ArrayList<>();


        if(input.trim().length() > 0) {
            objs.add("%" + input + "%");
            objs.add("%" + input + "%");
            clauses.add("(" + ScopedIdentifier.ID.access() + " IN ("
                    + " SELECT DISTINCT(" + Definition.SCOPED_ID.access() + ") FROM " + Definition.TABLE.from() + " WHERE "
                    + Definition.DEFINITION.access() + " ILIKE ? OR "
                    + Definition.DESIGNATION.access() + " ILIKE ?))");
        }

        if(!StringUtil.isEmpty(selectedNamespace)) {
            objs.add(selectedNamespace);
            clauses.add("(" + Misc.NAME.access() + " = ?)");
        }

        if(selectedTypes.length > 0) {
            clauses.add("(" + StringUtil.join(Arrays.asList(selectedTypes), " OR ", new Builder<ElementType>() {
                @Override
                public String build(ElementType o) {
                    return ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\"";
                }
            }) + ")");

            for(ElementType e : selectedTypes) {
                objs.add(e);
            }
        }

        if(selectedStatuses.length > 0) {
            clauses.add("(" + StringUtil.join(Arrays.asList(selectedStatuses), " OR ", new Builder<Status>() {
                @Override
                public String build(Status o) {
                    return ScopedIdentifier.STATUS.access() + " = ?::\"status\"";
                }
            }) + ")");

            for(Status e : selectedStatuses) {
                objs.add(e);
            }
        }

        if(slots.keySet().size() > 0) {
            StringBuilder subSql = new StringBuilder("SELECT " + Slot.SCOPED_ID.access() + " FROM ").append(SlotDAO.SQL_TABLE)
                    .append(" WHERE ");
            subSql.append(StringUtil.join(slots.keySet(), " OR ", new Builder<String>() {
                @Override
                public String build(String o) {
                    return "(" + Slot.KEY.access() + " = ? AND " + Slot.VALUE.access() + " = ?)";
                }
            }));

            for(Entry<String, String> s : slots.entrySet()) {
                objs.add(s.getValue());
                objs.add(s.getValue());
            }

            clauses.add("(" + ScopedIdentifier.ID.access() + " IN (" + subSql.toString() + "))");
        }

        StringBuilder sql = new StringBuilder(SQL_SELECT + " WHERE ");

        clauses.add(SQL_ACCESS_CLAUSE);
        objs.add(getUserId());

        if(clauses.size() > 0) {
            sql.append(StringUtil.join(clauses, " AND "));
        }

        return executeSelect(sql.toString(), objs.toArray());
    }

    /**
     * Returns a list of all available catalogs.
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getCatalogs() throws DAOException {
        init();

        StringBuilder sql = new StringBuilder(SQL_SELECT);
        sql.append(" WHERE " + ScopedIdentifier.ELEMENT_TYPE.access() + " = 'CATALOG' AND " + ScopedIdentifier.STATUS.access() + " = 'RELEASED' AND ");
        sql.append(SQL_ACCESS_CLAUSE);

        return executeSelect(sql.toString(), getUserId());
    }

    /**
     * Returns the specified element
     * @param urn
     * @return
     * @throws DAOException
     */
    public IdentifiedElement getElement(String urn) throws DAOException {
        init();

        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);

        if(identifier.getVersion().toLowerCase().equals("latest")) {
            return getLatestElement(urn);
        }

        StringBuilder sql = new StringBuilder(SQL_SELECT);

        sql.append(" WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                + ScopedIdentifier.VERSION.access() + " = ? AND "
                + Misc.NAME.access() + " = ? AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\"")
            .append(" AND ").append(SQL_ACCESS_CLAUSE);

        return executeSingleSelect(sql.toString(), identifier.getIdentifier(), identifier.getVersion(), identifier.getNamespace(),
                identifier.getElementType(), getUserId());
    }

    /**
     * Returns the element for the given scoped identifier ID.
     * @param scopedIdentifierId
     * @return
     * @throws DAOException
     */
    public IdentifiedElement getElement(int scopedIdentifierId) throws DAOException {
        init();

        StringBuilder sql = new StringBuilder(SQL_SELECT);

        sql.append(" WHERE " + ScopedIdentifier.ID.access() + " = ? AND ").append(SQL_ACCESS_CLAUSE);
        return executeSingleSelect(sql.toString(), scopedIdentifierId, getUserId());
    }

    /**
     * Returns the latest released element of the specified URN.
     */
    public IdentifiedElement getLatestElement(String urn) throws DAOException {
        ScopedIdentifier latestVersion = provider.get(ScopedIdentifierDAO.class).getLatestVersion(urn);

        if(latestVersion != null) {
            return getElement(latestVersion.toString());
        } else {
            return null;
        }
    }

    /**
     * Returns all elements that are currently in a draft status and writable by the user.
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getDrafts() throws DAOException {
        init();

        StringBuilder sql = new StringBuilder(SQL_SELECT);
        sql.append(" WHERE " + ScopedIdentifier.STATUS.access() + " = 'DRAFT' AND "
                + Element.ELEMENT_TYPE.access() + " <> 'CODE' AND "
                + ScopedIdentifier.NAMESPACE_ID.access() + " IN (SELECT " + UserWritableNamespace.NAMESPACE_ID.access() + " FROM "
                + UserWritableNamespace.TABLE.from() + " WHERE " + UserWritableNamespace.USER_ID.access() + " = ?) AND " + SQL_ACCESS_CLAUSE);
        return executeSelect(sql.toString(), getUserId(), getUserId());
    }

    /**
     * Returns all root elements that aren't in a group, catalog, or code.
     * @param namespace
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getRootElements(String namespace) throws DAOException {
        init();

        StringBuilder sql = new StringBuilder(SQL_SELECT);
        sql.append(" WHERE " + Misc.NAME.access() + " = ? AND ")
            .append(ScopedIdentifier.STATUS.access()).append(" = 'RELEASED' AND ")
            .append(ScopedIdentifier.ID.access()).append(" NOT IN (SELECT ").append(ScopedHierarchy.SUB.access()).append(" FROM ")
                .append(ScopedHierarchy.TABLE.from())
                .append(" LEFT JOIN ").append(ScopedIdentifier.TABLE.from())
                    .append(" ON ").append(ScopedIdentifier.ID.access()).append(" = ").append(ScopedHierarchy.SUPER.access())
                .append(" LEFT JOIN ").append(Misc.NS.from())
                    .append(" ON ").append(Misc.ID.access()).append(" = ").append(ScopedIdentifier.NAMESPACE_ID.access())
                .append(" WHERE ").append(Misc.NAME.access()).append(" = ? AND ")
                .append(ScopedIdentifier.STATUS.access()).append(" = 'RELEASED' AND (")
                    .append(ScopedIdentifier.ELEMENT_TYPE.access()).append(" = 'DATAELEMENTGROUP' OR")
                    .append(ScopedIdentifier.ELEMENT_TYPE.access()).append(" = 'CATALOG' OR")
                    .append(ScopedIdentifier.ELEMENT_TYPE.access()).append(" = 'CODE'))")
            .append(" AND ").append(SQL_ACCESS_CLAUSE);
        return executeSelect(sql.toString(), namespace, namespace, getUserId());
    }

    /**
     * Returns a list of identified elements that described the same element as the specified URN.
     * @param urn
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getSimilarElements(String urn) throws DAOException {
        init();

        ScopedIdentifier identifier = provider.get(ScopedIdentifierDAO.class).getScopedIdentifier(urn);

        StringBuilder sql = new StringBuilder(SQL_SELECT);
        sql.append(" WHERE " + ScopedIdentifier.ELEMENT_ID.access() + " = ? AND " + ScopedIdentifier.UUID.access() + " != ?")
           .append(" AND " + ScopedIdentifier.STATUS.access() + " = 'RELEASED' AND " + SQL_ACCESS_CLAUSE);

        return executeSelect(sql.toString(), identifier.getElementId(), identifier.getUuid(), getUserId());
    }

    /**
     * Finds the Owned represetations for the given URN with the given statuses.
     * @param urn
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> findOwnedRepresentations(String urn, Status... status) throws DAOException {
        init();

        ScopedIdentifier identifier = provider.get(ScopedIdentifierDAO.class).getScopedIdentifier(urn);

        List<Object> objs = new ArrayList<>();
        List<String> clauses = new ArrayList<>();

        clauses.add(ScopedIdentifier.ELEMENT_ID.access() + " = ?");
        objs.add(identifier.getElementId());

        clauses.add(ScopedIdentifier.NAMESPACE_ID.access() + " IN "
           + "(SELECT " + Misc.UserWritableNamespace.NAMESPACE_ID.access() + " FROM " + Misc.UserWritableNamespace.TABLE.from() + " WHERE "
                   + Misc.UserWritableNamespace.USER_ID.access() + " = ?)");
        objs.add(getUserId());

        if(status != null && status.length > 0) {
            clauses.add("(" + StringUtil.join(Arrays.asList(status), " OR ", new Builder<Status>() {
                @Override
                public String build(Status o) {
                    return ScopedIdentifier.STATUS.access() + " = ?::\"status\"";
                }
            }) + ")");

            for(Status e : status) {
                objs.add(e);
            }
        }

        clauses.add(SQL_ACCESS_CLAUSE);
        objs.add(getUserId());

        StringBuilder sql = new StringBuilder(SQL_SELECT);
        sql.append(" WHERE " );
        sql.append(StringUtil.join(clauses, " AND "));

        return executeSelect(sql.toString(), objs.toArray());
    }

    @Override
    public IdentifiedElement getObject(ResultSet set) throws SQLException {
        if(indexElement.containsKey(set.getInt(ScopedIdentifier.ID.alias))) {
            indexElement.get(set.getInt(ScopedIdentifier.ID.alias)).getDefinitions().add(provider.get(DefinitionDAO.class).getObject(set));
            return null;
        } else {
            IdentifiedElement identified = new IdentifiedElement();
            identified.setScoped(provider.get(ScopedIdentifierDAO.class).getObject(set));

            identified.setElement(provider.get(ElementDAO.class).getObject(set));

            identified.setDefinitions(new ArrayList<Definition>());

            if(set.getInt(Definition.ID.alias) != 0) {
                identified.getDefinitions().add(provider.get(DefinitionDAO.class).getObject(set));
            }

            indexElement.put(identified.getScoped().getId(), identified);

            return identified;
        }
    }

    /**
     * Returns all members from the given URN.
     * @param urn
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getSubMembers(String urn) throws DAOException {
        init();
        return getEntries(urn);
    }

    /**
     * Returns all entries (= ordered members) from the given URN.
     * @param urn
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getEntries(String urn) throws DAOException {
        init();

        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);

        StringBuilder sql = new StringBuilder("SELECT " + SQL_SELECT_FIELDS + ", " + getSelectFields(ScopedHierarchy.class)
                + SQL_SELECT_FROM + " LEFT JOIN " + ScopedHierarchy.TABLE.from() + " ON " + ScopedHierarchy.SUB.access() + " = " + ScopedIdentifier.ID.access()
                + " LEFT JOIN \"scopedIdentifier\" AS sc2 ON h.\"superId\" = sc2.\"id\" "
                + " LEFT JOIN \"element\" AS n2 ON n2.id = sc2.\"namespaceId\" "
                + " WHERE sc2.\"identifier\" = ? "
                + " AND sc2.\"version\" = ? AND sc2.\"elementType\" = ?::\"elementType\" AND n2.\"name\" = ?"
                + " AND " + SQL_ACCESS_CLAUSE + " ORDER BY h.\"order\"");

        return executeSelect(sql.toString(), identifier.getIdentifier(), identifier.getVersion(),
                identifier.getElementType(), identifier.getNamespace(), getUserId());
    }

    /**
     * Returns all submembers of the given URN. Includes really *all* descendants.
     * @param urn
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getAllSubMembers(String urn) throws DAOException {
        init();

        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);

        StringBuilder sql = new StringBuilder(SQL_SELECT)
                .append(" WHERE " + ScopedIdentifier.ID.access() + " IN "
                        + "(SELECT " + HierarchyNode.SUB.access() + " FROM " + HierarchyNode.TABLE.from() + " LEFT JOIN " + ScopedIdentifier.TABLE.from())
                .append(" ON " + ScopedIdentifier.ID.access() + " = " + HierarchyNode.ROOT.access())
                .append(" LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access())
                .append(" WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? AND ")
                .append(ScopedIdentifier.VERSION.access() + " = ? AND ")
                .append(ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND ")
                .append(Misc.NAME.access() + " = ?) AND ").append(SQL_ACCESS_CLAUSE);

        return executeSelect(sql.toString(), identifier.getIdentifier(), identifier.getVersion(),
                identifier.getElementType(), identifier.getNamespace(), getUserId());
    }

    /**
     * Returns all permissible codes for the given value domain.
     * @param valueDomain
     * @return
     * @throws DAOException
     */
    public List<IdentifiedElement> getPermissibleCodes(ValueDomain valueDomain) throws DAOException {
        init();

        StringBuilder sql = new StringBuilder(SQL_SELECT)
                .append(" WHERE " + ScopedIdentifier.ID.access() + " IN "
                        + "(SELECT " + PermissibleCode.CODE_SCOPED_ID.access() + " FROM " + PermissibleCode.TABLE.from()
                        + " WHERE " + PermissibleCode.CATALOG_VD_ID.access() + " = ?) AND ")
                .append(SQL_ACCESS_CLAUSE);

        return executeSelect(sql.toString(), valueDomain.getId(), getUserId());
    }

    private void init() {
        indexElement = new HashMap<>();
        provider.get(NamespaceDAO.class).init();
    }

    @Override
    protected String getInsertTable() {
        return null;
    }

    @Override
    protected String getInsertFields() {
        return null;
    }

    /**
     * The fields used for queries for described elements (e.g. namespaces are not identified, but described).
     */
    public static final String SQL_DESCRIBED_FIELDS = DefinitionDAO.SQL_SELECT_FIELDS + ", " + ElementDAO.SQL_SELECT_FIELDS;

    /**
     * The fields used for queries for identified elements.
     */
    public static final String SQL_SELECT_FIELDS = ScopedIdentifierDAO.SQL_SELECT_FIELDS + ", " + SQL_DESCRIBED_FIELDS;

    /**
     * The joins for the definitions.
     */
    public static final String SQL_JOINS =
            " LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() +
            " LEFT JOIN " + ElementDAO.SQL_TABLE + " ON " + Element.ID.access()+ " = " + ScopedIdentifier.ELEMENT_ID.access() +
            " LEFT JOIN " + DefinitionDAO.SQL_TABLE + " ON "
                    + "(" + Definition.ELEMENT_ID.access() + " = " + Element.ID.access() + " AND "
                          + Definition.SCOPED_ID.access() + " = " + ScopedIdentifier.ID.access() + ")";

    public static final String SQL_SELECT_FROM = " FROM " + ScopedIdentifierDAO.SQL_TABLE + SQL_JOINS;

    public static final String SQL_SELECT = "SELECT " + SQL_SELECT_FIELDS + SQL_SELECT_FROM;

    /**
     * This access clause prevents that the user sees elements, that he should not be able to see (missing permissions).
     */
    public static final String SQL_ACCESS_CLAUSE = "(" + Misc.HIDDEN.access() + " = false OR " + Misc.ID.access() + " IN "
            + "(SELECT " + UserReadableNamespace.NAMESPACE_ID.access() + " FROM "
                    + UserReadableNamespace.TABLE.from() + " WHERE " + UserReadableNamespace.USER_ID.access() + " = ?))";


}
