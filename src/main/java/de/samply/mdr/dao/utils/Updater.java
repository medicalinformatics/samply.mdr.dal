/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.sdao.DAOException;

/**
 * A helper class that is used to update a group.
 */
public class Updater {

    private final MDRConnection mdr;

    private final String urn;

    private final int userId;

    private final HashMap<IdentifiedElement, IdentifiedElement> rootReplacements;

    public Updater(MDRConnection mdr, String urn, int userId) {
        this.mdr = mdr;
        this.urn = urn;
        this.userId = userId;
        this.rootReplacements = new HashMap<>();
    }

    public IdentifiedElement update() throws DAOException {
        return updateElement(urn);
    }

    /**
     * Updates the given URN. That means this method searches for new identifiers for elements.
     * @param urn
     * @return
     * @throws DAOException
     */
    private IdentifiedElement updateElement(String urn) throws DAOException {
        IdentifiedDAO elementDAO = mdr.get(IdentifiedDAO.class);
        IdentifiedElement element = elementDAO.getElement(urn);

        if(element.getScoped().getStatus() == Status.OUTDATED) {
            element = elementDAO.getLatestElement(urn);

            if(element == null) {
                return null;
            }
        }

        if(element.getElementType() != ElementType.DATAELEMENTGROUP) {
            return element;
        }

        HashMap<IdentifiedElement, IdentifiedElement> replacer = new HashMap<IdentifiedElement, IdentifiedElement>();

        List<IdentifiedElement> subMembers = elementDAO.getSubMembers(urn);

        for(IdentifiedElement subMember : subMembers) {
            IdentifiedElement newMember = updateElement(subMember.getScoped().toString());

            if(newMember == null) {
                replacer.put(subMember, null);
            } else if(newMember.getScoped().getId() != subMember.getScoped().getId()) {
                replacer.put(subMember, newMember);
            }
        }

        if(replacer.size() == 0) {
            return element;
        } else {
            rootReplacements.putAll(replacer);

            /**
             * Create a new group, copy all definitions
             */
            DataElementGroup group = new DataElementGroup();
            mdr.get(ElementDAO.class).saveElement(group);

            ScopedIdentifierDAO scopedDAO = mdr.get(ScopedIdentifierDAO.class);
            DefinitionDAO definitionDAO = mdr.get(DefinitionDAO.class);

            ScopedIdentifier identifier = new ScopedIdentifier();
            identifier.setElementType(ElementType.DATAELEMENTGROUP);
            identifier.setNamespace(element.getScoped().getNamespace());
            identifier.setNamespaceId(element.getScoped().getNamespaceId());
            identifier.setIdentifier(element.getScoped().getIdentifier());
            identifier.setVersion("" + (Integer.parseInt(element.getScoped().getVersion()) + 1));
            identifier.setElementId(group.getId());
            identifier.setCreatedBy(userId);
            identifier.setUrl("none");
            identifier.setStatus(Status.RELEASED);

            scopedDAO.saveScopedIdentifier(identifier);

            for(Definition def : element.getDefinitions()) {
                Definition newDef = new Definition();
                newDef.setDefinition(def.getDefinition());
                newDef.setDesignation(def.getDesignation());
                newDef.setElementId(group.getId());
                newDef.setScopedIdentifierId(identifier.getId());
                newDef.setLanguage(def.getLanguage());
                definitionDAO.saveDefinition(newDef);
            }

            int index = 1;
            for(IdentifiedElement subMember : subMembers) {
                if(replacer.containsKey(subMember)) {
                    if(replacer.get(subMember) != null) {
                        scopedDAO.addSubIdentifier(identifier.getId(), replacer.get(subMember).getScoped().getId(), index++);
                    }
                } else {
                    scopedDAO.addSubIdentifier(identifier.getId(), subMember.getScoped().getId(), index++);
                }
            }

            scopedDAO.outdateIdentifier(element.getScoped().toString());

            return elementDAO.getElement(identifier.toString());
        }
    }

    /**
     * @return the rootReplacer
     */
    public Map<IdentifiedElement, IdentifiedElement> getRootReplacements() {
        return Collections.unmodifiableMap(rootReplacements);
    }

}
