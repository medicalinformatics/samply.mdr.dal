/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBElement;

import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.PermissibleCodeDAO;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.SlotDAO;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.CatalogValueDomain;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.DataElement;
import de.samply.mdr.xsd.DataElementGroup;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.DescribedValueDomain;
import de.samply.mdr.xsd.Element;
import de.samply.mdr.xsd.EnumeratedValueDomain;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.PermissibleValue;
import de.samply.mdr.xsd.Record;
import de.samply.mdr.xsd.ScopedIdentifier;
import de.samply.mdr.xsd.Slot;
import de.samply.sdao.DAOException;

/**
 * The Importer imports a URN into another namspace. If the URN is a group or a record, all subelements
 * will be imported too.
 * @author paul
 *
 */
public class Importer {

    /**
     * The Map that maps all already imported URNs to their new URNs.
     */
    private HashMap<String, String> mapper = new HashMap<>();

    private HashMap<String, ImportStrategy> strategy = new HashMap<>();

    private HashMap<String, Element> elementMap = new HashMap<>();

    private HashMap<String, Object> index = new HashMap<>();

    private HashMap<String, HashMap<String, ScopedIdentifier>> identifierMap = new HashMap<>();

    private List<Import> imports = new ArrayList<>();

    /**
     * Imports the given URN into the given namespace. Imports all subelements into the given namespace as well.
     *
     * @param mdr the MDR connection
     * @param urn the URN that will be imported
     * @param newNamespace the new namespace
     * @param userId the current user ID
     * @return
     * @throws DAOException
     */
    public de.samply.mdr.dal.dto.ScopedIdentifier importElement(MDRConnection mdr, String urn, String newNamespace, int userId) throws DAOException {

        if(mapper.containsKey(urn)) {
            return mdr.get(ScopedIdentifierDAO.class).getScopedIdentifier(mapper.get(urn));
        }

        de.samply.mdr.dal.dto.ScopedIdentifier scoped = de.samply.mdr.dal.dto.ScopedIdentifier.fromURN(urn);

        if(scoped.getNamespace().equals(newNamespace)) {
            return mdr.get(ScopedIdentifierDAO.class).getScopedIdentifier(urn);
        }

        de.samply.mdr.dal.dto.ScopedIdentifier newScoped = mdr.importElement(urn, newNamespace, userId);
        mapper.put(urn, newScoped.toString());

        switch(scoped.getElementType()) {
        case DATAELEMENTGROUP:
            for(IdentifiedElement element : mdr.get(IdentifiedDAO.class).getSubMembers(urn)) {
                de.samply.mdr.dal.dto.ScopedIdentifier sub = importElement(mdr, element.getScoped().toString(), newNamespace, userId);
                mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(newScoped.getId(), sub.getId());
            }
            break;

        case RECORD:
            int index = 1;
            for(IdentifiedElement element : mdr.get(IdentifiedDAO.class).getEntries(urn)) {
                de.samply.mdr.dal.dto.ScopedIdentifier sub = importElement(mdr, element.getScoped().toString(), newNamespace, userId);
                mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(newScoped.getId(), sub.getId(), index++);
            }
            break;

        default:
            break;
        }

        return newScoped;
    }


    /**
     * @param export
     * @throws DAOException
     */
    public void importExport(Export export, List<ImportStrategy> strategy, MDRConnection mdr) throws DAOException {
        for(ImportStrategy imp : strategy) {
            this.strategy.put(imp.from, imp);
        }

        for(JAXBElement<? extends Element> jaxbElement : export.getElement()) {
            Element element = jaxbElement.getValue();

            elementMap.put(element.getUuid().toString(), element);

            if(element instanceof ScopedIdentifier) {
                put((ScopedIdentifier) element);
            }
        }

        for(JAXBElement<? extends Element> jaxbElement : export.getElement()) {
            Element element = jaxbElement.getValue();

            importElement(element, mdr);
        }

    }

    /**
     * @param element
     */
    private void put(ScopedIdentifier element) {
        if(! identifierMap.containsKey(element.getNamespace())) {
            identifierMap.put(element.getNamespace(), new HashMap<String, ScopedIdentifier>());
        }

        identifierMap.get(element.getNamespace()).put(element.getUuid(), element);
    }

    private ScopedIdentifier get(String uuidNamespace, String uuidIdentifier) {
        return identifierMap.get(uuidNamespace).get(uuidIdentifier);
    }

    private Object importElement(Element element, MDRConnection mdr) throws DAOException {
        if(index.containsKey(element.getUuid().toString())) {
            return index.get(element.getUuid().toString());
        }

        /**
         * If this element is already in the database, use it.
         * This will not work for scoped identifiers, those must follow the
         * import strategy!
         */
        de.samply.mdr.dal.dto.Element existing = mdr.get(ElementDAO.class).getElement(UUID.fromString(element.getUuid()));

        if(existing != null) {
            index.put(element.getUuid(), existing);
        } else if(element instanceof Namespace) {
            System.out.println("Ignoring namespace!");
        } else if(element instanceof DataElement) {
            DataElement de = (DataElement) element;
            importDataElement(de, mdr);
        } else if(element instanceof Catalog) {
            Catalog catalog = (Catalog) element;
            importCatalog(catalog, mdr);
        } else if(element instanceof Code) {
            Code code = (Code) element;
            importCode(code, mdr);
        } else if(element instanceof DataElementGroup) {
            DataElementGroup group = (DataElementGroup) element;
            importDataElementGroup(group, mdr);
        } else if(element instanceof Record) {
            Record record = (Record) element;
            importRecord(record, mdr);
        } else if(element instanceof EnumeratedValueDomain) {
            EnumeratedValueDomain enumerated = (EnumeratedValueDomain) element;
            importEnumeratedValueDomain(enumerated, mdr);
        } else if(element instanceof DescribedValueDomain) {
            DescribedValueDomain described = (DescribedValueDomain) element;
            importDescribedValueDomain(described, mdr);
        } else if(element instanceof CatalogValueDomain) {
            CatalogValueDomain catalogVd = (CatalogValueDomain) element;
            importCatalogValueDomain(catalogVd, mdr);
        } else if(element instanceof PermissibleValue) {
            PermissibleValue value = (PermissibleValue) element;
            importPermissibleValue(value, mdr);
        } else if(element instanceof ScopedIdentifier) {
            ScopedIdentifier identifier = (ScopedIdentifier) element;
            importScopedIdentifier(identifier, mdr);
        }

        return index.get(element.getUuid());
    }

    /**
     * @param identifier
     * @param mdr
     * @throws DAOException
     */
    private void importScopedIdentifier(ScopedIdentifier identifier, MDRConnection mdr) throws DAOException {
        /**
         * There are two types of scoped identifiers that exist exactly once:
         * scoped identifiers for codes and catalogs. So check if the already exist in the
         * database.
         */
        Element referencedElement = elementMap.get(identifier.getElement());
        Namespace namespace = (Namespace) elementMap.get(identifier.getNamespace());
        String targetNamespace = strategy.get(namespace.getName()).to;

        Object imported = importElement(referencedElement, mdr);

        if(referencedElement instanceof Catalog || referencedElement instanceof Code) {
//            de.samply.mdr.dal.dto.ScopedIdentifier existingIdentifier =
//                    mdr.get(ScopedIdentifierDAO.class).getScopedIdentifier(identifier.getUuid());
//
//            if(existingIdentifier != null) {
//                index.put(identifier.getUuid(), existingIdentifier);
//            } else {
//                /**
//                 * This part is tricky: search the database for an identifier that references the catalog.
//                 * Since this exact scoped identifier does not exist, try to find an existing one that references
//                 * the catalog/code. If there is one, increment the version and import it.
//                 */
//            }
            throw new UnsupportedOperationException("Import and export of catalogs is not supported yet!");
        } else {
            de.samply.mdr.dal.dto.ScopedIdentifier existingIdentifier =
                mdr.get(ScopedIdentifierDAO.class).getScopedIdentifier(
                    UUID.fromString(identifier.getUuid()), targetNamespace);

            /**
             * If the scoped identifier already exists in the target namespace, return it.
             */
            if(existingIdentifier != null) {
                index.put(identifier.getUuid(), existingIdentifier);
            } else {

                de.samply.mdr.dal.dto.Element importedElement = (de.samply.mdr.dal.dto.Element) imported;

                de.samply.mdr.dal.dto.ScopedIdentifier newIdentifier = new de.samply.mdr.dal.dto.ScopedIdentifier();
                newIdentifier.setCreatedBy(mdr.getUserId());
                newIdentifier.setElementId(importedElement.getId());
                newIdentifier.setElementType(importedElement.getElementType());
                newIdentifier.setStatus(Status.DRAFT);
                newIdentifier.setUrl("none");
                newIdentifier.setUuid(UUID.fromString(identifier.getUuid()));
                newIdentifier.setNamespace(targetNamespace);

                /**
                 * Find an identifier that references the referencedElement in the target namespace
                 */
                List<de.samply.mdr.dal.dto.ScopedIdentifier> foundIdentifiers = mdr.get(ScopedIdentifierDAO.class)
                        .getScopedIdentifierForElement(UUID.fromString(referencedElement.getUuid()), targetNamespace);

                /**
                 * So there is at least one, find a new identifier and save it!
                 */
                if(foundIdentifiers.size() > 0) {
                    int newRevision = mdr.get(ScopedIdentifierDAO.class).getNewRevision(foundIdentifiers.get(0).toString());
                    newIdentifier.setVersion("" + newRevision);
                    newIdentifier.setIdentifier(foundIdentifiers.get(0).getIdentifier());
                    newIdentifier.setNamespaceId(foundIdentifiers.get(0).getNamespaceId());
                } else {
                    newIdentifier.setVersion("1");
                    newIdentifier.setIdentifier(mdr.get(ScopedIdentifierDAO.class).getFreeIdentifier(targetNamespace, importedElement.getElementType()));
                    newIdentifier.setNamespaceId(mdr.get(NamespaceDAO.class).getNamespace(targetNamespace).getElement().getId());
                }
                mdr.get(ScopedIdentifierDAO.class).saveScopedIdentifier(newIdentifier);

                /**
                 * Save all definitions
                 */
                for(Definition def : identifier.getDefinitions().getDefinition()) {
                    de.samply.mdr.dal.dto.Element referencedImportedElement = (de.samply.mdr.dal.dto.Element) importElement(elementMap.get(def.getUuid()), mdr);

                    de.samply.mdr.dal.dto.Definition newDefinition = new de.samply.mdr.dal.dto.Definition();
                    newDefinition.setDefinition(def.getDefinition());
                    newDefinition.setDesignation(def.getDesignation());
                    newDefinition.setLanguage(def.getLang());
                    newDefinition.setScopedIdentifierId(newIdentifier.getId());
                    newDefinition.setElementId(referencedImportedElement.getId());
                    mdr.get(DefinitionDAO.class).saveDefinition(newDefinition);
                }

                /**
                 * Save all slots
                 */
                for(Slot slot : identifier.getSlots().getSlot()) {
                    de.samply.mdr.dal.dto.Slot newSlot = new de.samply.mdr.dal.dto.Slot();
                    newSlot.setKey(slot.getKey());
                    newSlot.setValue(slot.getValue());
                    newSlot.setScopedIdentifierId(newIdentifier.getId());
                    mdr.get(SlotDAO.class).saveSlot(newSlot);
                }

                /**
                 * Save the hierarchy
                 */
                int order = 1;
                for(String sub : identifier.getSub()) {
                    de.samply.mdr.dal.dto.ScopedIdentifier subIdentifier = (de.samply.mdr.dal.dto.ScopedIdentifier)
                            importElement(get(identifier.getNamespace(), sub), mdr);

                    mdr.get(ScopedIdentifierDAO.class).addSubIdentifier(newIdentifier.getId(), subIdentifier.getId(), order++);
                }

                String sourceNamespace = ((Namespace) elementMap.get(identifier.getNamespace())).getName();
                Import imp = new Import();
                imp.setTo(newIdentifier.toString());
                imp.setFrom("urn:" + sourceNamespace + ":" + newIdentifier.getElementType().toString().toLowerCase() + ":" + identifier.getIdentifier() +
                        ":" + identifier.getVersion());
                imports.add(imp);

                index.put(identifier.getUuid(), newIdentifier);
            }
        }
    }


    /**
     * @param value
     * @param mdr
     * @throws DAOException
     */
    private void importPermissibleValue(PermissibleValue value, MDRConnection mdr) throws DAOException {
        ValueDomain valueDomain = (ValueDomain) importElement(elementMap.get(value.getValueDomain()), mdr);

        de.samply.mdr.dal.dto.PermissibleValue newValue = new de.samply.mdr.dal.dto.PermissibleValue();
        newValue.setUuid(UUID.fromString(value.getUuid()));
        newValue.setElementType(ElementType.PERMISSIBLE_VALUE);
        newValue.setPermittedValue(value.getValue());
        newValue.setValueDomainId(valueDomain.getId());

        mdr.get(ElementDAO.class).saveElement(newValue);

        index.put(value.getUuid(), newValue);
    }


    /**
     * @param catalogVd
     * @param mdr
     * @throws DAOException
     */
    private void importCatalogValueDomain(CatalogValueDomain catalogVd, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.ScopedIdentifier catalogIdentifier = (de.samply.mdr.dal.dto.ScopedIdentifier)
                importElement(elementMap.get(catalogVd.getCatalog()), mdr);

        de.samply.mdr.dal.dto.CatalogValueDomain newCatalogVd = new de.samply.mdr.dal.dto.CatalogValueDomain();
        newCatalogVd.setDatatype(catalogVd.getDatatype());
        newCatalogVd.setFormat(catalogVd.getFormat());
        newCatalogVd.setMaxCharacters(catalogVd.getMaxCharacters().intValue());
        newCatalogVd.setUnitOfMeasure(catalogVd.getUnitOfMeasure());
        newCatalogVd.setCatalogScopedIdentifierId(catalogIdentifier.getId());

        newCatalogVd.setElementType(ElementType.ENUMERATED_VALUE_DOMAIN);
        newCatalogVd.setUuid(UUID.fromString(catalogVd.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newCatalogVd);

        index.put(catalogVd.getUuid(), newCatalogVd);

        for(String permissibleCode : catalogVd.getPermissibleCode()) {
            de.samply.mdr.dal.dto.ScopedIdentifier code = (de.samply.mdr.dal.dto.ScopedIdentifier) importElement(elementMap.get(permissibleCode), mdr);

            PermissibleCode newPermissibleCode = new PermissibleCode();
            newPermissibleCode.setCatalogValueDomainId(newCatalogVd.getId());
            newPermissibleCode.setCodeScopedIdentifierId(code.getId());

            mdr.get(PermissibleCodeDAO.class).savePermissibleCode(newPermissibleCode);
        }
    }


    /**
     * @param described
     * @param mdr
     * @throws DAOException
     */
    private void importDescribedValueDomain(DescribedValueDomain described, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.DescribedValueDomain newDescribed = new de.samply.mdr.dal.dto.DescribedValueDomain();
        newDescribed.setDatatype(described.getDatatype());
        newDescribed.setFormat(described.getFormat());
        newDescribed.setMaxCharacters(described.getMaxCharacters().intValue());
        newDescribed.setUnitOfMeasure(described.getUnitOfMeasure());

        newDescribed.setDescription(described.getDescription());
        newDescribed.setValidationData(described.getValidationData());
        newDescribed.setValidationType(ValidationType.valueOf(described.getValidationType()));

        newDescribed.setElementType(ElementType.ENUMERATED_VALUE_DOMAIN);
        newDescribed.setUuid(UUID.fromString(described.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newDescribed);

        index.put(described.getUuid(), newDescribed);
    }


    /**
     * @param enumerated
     * @param mdr
     * @throws DAOException
     */
    private void importEnumeratedValueDomain(EnumeratedValueDomain enumerated, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.EnumeratedValueDomain newEnumerated = new de.samply.mdr.dal.dto.EnumeratedValueDomain();
        newEnumerated.setDatatype(enumerated.getDatatype());
        newEnumerated.setFormat(enumerated.getFormat());
        newEnumerated.setMaxCharacters(enumerated.getMaxCharacters().intValue());
        newEnumerated.setUnitOfMeasure(enumerated.getUnitOfMeasure());

        newEnumerated.setElementType(ElementType.ENUMERATED_VALUE_DOMAIN);
        newEnumerated.setUuid(UUID.fromString(enumerated.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newEnumerated);

        index.put(enumerated.getUuid(), newEnumerated);
    }


    /**
     * @param record
     * @param mdr
     * @throws DAOException
     */
    private void importRecord(Record record, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.Record newRecord = new de.samply.mdr.dal.dto.Record();
        newRecord.setElementType(ElementType.RECORD);
        newRecord.setUuid(UUID.fromString(record.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newRecord);

        index.put(record.getUuid(), newRecord);
    }


    /**
     * @param group
     * @param mdr
     * @throws DAOException
     */
    private void importDataElementGroup(DataElementGroup group, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.DataElementGroup newGroup = new de.samply.mdr.dal.dto.DataElementGroup();
        newGroup.setElementType(ElementType.DATAELEMENTGROUP);
        newGroup.setUuid(UUID.fromString(group.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newGroup);

        index.put(group.getUuid(), newGroup);
    }


    /**
     * @param code
     * @param mdr
     * @throws DAOException
     */
    private void importCode(Code code, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.Catalog catalog = (de.samply.mdr.dal.dto.Catalog) importElement(elementMap.get(code.getCatalog()), mdr);

        de.samply.mdr.dal.dto.Code newCode = new de.samply.mdr.dal.dto.Code();
        newCode.setCatalogId(catalog.getId());
        newCode.setCode(code.getCode());
        newCode.setValid(code.isIsValid());

        newCode.setElementType(ElementType.CODE);
        newCode.setUuid(UUID.fromString(code.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newCode);

        index.put(code.getUuid(), newCode);
    }


    /**
     * @param catalog
     * @param mdr
     * @throws DAOException
     */
    private void importCatalog(Catalog catalog, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.Catalog newCatalog = new de.samply.mdr.dal.dto.Catalog();
        newCatalog.setUuid(UUID.fromString(catalog.getUuid()));
        newCatalog.setElementType(ElementType.CATALOG);

        mdr.get(ElementDAO.class).saveElement(newCatalog);

        index.put(catalog.getUuid(), newCatalog);
    }


    /**
     * @param de
     * @throws DAOException
     */
    private void importDataElement(DataElement de, MDRConnection mdr) throws DAOException {
        de.samply.mdr.dal.dto.ValueDomain domain = (ValueDomain) importElement(elementMap.get(de.getValueDomain()), mdr);

        de.samply.mdr.dal.dto.DataElement newDataElement = new de.samply.mdr.dal.dto.DataElement();
        newDataElement.setElementType(ElementType.DATAELEMENT);
        newDataElement.setValueDomainId(domain.getId());
        newDataElement.setUuid(UUID.fromString(de.getUuid()));

        mdr.get(ElementDAO.class).saveElement(newDataElement);

        index.put(de.getUuid(), newDataElement);
    }

    /**
     * @return the imports
     */
    public List<Import> getImports() {
        return imports;
    }

    public static class ImportStrategy implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        private String from;

        private String to;

        /**
         * @return the from
         */
        public String getFrom() {
            return from;
        }

        /**
         * @param from the from to set
         */
        public void setFrom(String from) {
            this.from = from;
        }

        /**
         * @return the to
         */
        public String getTo() {
            return to;
        }

        /**
         * @param to the to to set
         */
        public void setTo(String to) {
            this.to = to;
        }

    }

    public static class Import implements Serializable {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        private String from;

        private String to;

        /**
         * @return the from
         */
        public String getFrom() {
            return from;
        }

        /**
         * @param from the from to set
         */
        public void setFrom(String from) {
            this.from = from;
        }

        /**
         * @return the to
         */
        public String getTo() {
            return to;
        }

        /**
         * @param to the to to set
         */
        public void setTo(String to) {
            this.to = to;
        }
    }


}
