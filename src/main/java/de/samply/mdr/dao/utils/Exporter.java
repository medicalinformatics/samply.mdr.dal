package de.samply.mdr.dao.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBElement;

import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.SlotDAO;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.CatalogValueDomain;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.DataElement;
import de.samply.mdr.xsd.DataElementGroup;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Definitions;
import de.samply.mdr.xsd.DescribedValueDomain;
import de.samply.mdr.xsd.EnumeratedValueDomain;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.PermissibleValue;
import de.samply.mdr.xsd.Record;
import de.samply.mdr.xsd.ScopedIdentifier;
import de.samply.mdr.xsd.Slot;
import de.samply.mdr.xsd.Slots;
import de.samply.sdao.DAOException;

/**
 *
 */
public class Exporter {

    private List<DescribedElement> elements = new ArrayList<>();

    private final MDRConnection mdr;

    private HashMap<String, DescribedElement> namespaceMap = new HashMap<>();

    private HashSet<UUID> exported = new HashSet<>();

    private Export target = new Export();

    ObjectFactory factory = new ObjectFactory();

    public Exporter(MDRConnection mdr) {
        this.mdr = mdr;
    }

    public void add(DescribedElement element) {

        if(element instanceof DescribedElement) {
            elements.add(element);
        } else {
            System.out.println("Exporting something unknown!");
        }

    }

    public Export generateExport() throws DAOException {
        for(DescribedElement element : elements) {
            if(element instanceof IdentifiedElement) {
                IdentifiedElement identified = (IdentifiedElement) element;

                if(! exported.contains(identified.getScoped().getUuid())) {
                    export(identified);
                }
            } else if(element instanceof DescribedElement) {
                de.samply.mdr.dal.dto.Namespace namespace = (de.samply.mdr.dal.dto.Namespace) element.getElement();
                for(IdentifiedElement e : mdr.get(IdentifiedDAO.class).getRootElements(namespace.getName())) {
                    if(! exported.contains(e.getScoped().getUuid())) {
                        export(e);
                    }
                }
            }
        }

        return target;
    }

    private void export(de.samply.mdr.dal.dto.IdentifiedElement identified) throws DAOException {

        if(exported.contains(identified.getScoped().getUuid())) {
            return;
        }

        de.samply.mdr.dal.dto.Namespace ns = getNamespace(identified.getScoped().getNamespace());
        Element element = identified.getElement();

        ScopedIdentifier exp = new ScopedIdentifier();
        exp.setIdentifier(identified.getScoped().getIdentifier());
        exp.setNamespace(ns.getUuid().toString());
        exp.setUuid(identified.getScoped().getUuid().toString());
        exp.setVersion(identified.getScoped().getVersion());
        exp.setElement(element.getUuid().toString());

        exp.setDefinitions(new Definitions());
        exp.getDefinitions().getDefinition().addAll(convert(identified.getDefinitions(), identified.getElement().getUuid()));

        List<de.samply.mdr.dal.dto.Slot> slots = mdr.get(SlotDAO.class).getSlots(identified.getScoped().toString());
        exp.setSlots(new Slots());
        for(de.samply.mdr.dal.dto.Slot slot : slots) {
            Slot expSlot = new Slot();
            expSlot.setKey(slot.getKey());
            expSlot.setValue(slot.getValue());
            exp.getSlots().getSlot().add(expSlot);
        }

        if(! exported.contains(ns.getUuid())) {
            export(ns);
        }

        if(element instanceof de.samply.mdr.dal.dto.DataElementGroup) {
            for(IdentifiedElement e : mdr.get(IdentifiedDAO.class).getSubMembers(identified.getScoped().toString())) {
                export(e);

                exp.getSub().add(e.getScoped().getUuid().toString());
            }

            DataElementGroup expGroup = new DataElementGroup();
            expGroup.setUuid(element.getUuid().toString());
            add(factory.createDataElementGroup(expGroup));
        } else if(element instanceof de.samply.mdr.dal.dto.DataElement) {
            DataElement expEl = new DataElement();
            expEl.setUuid(element.getUuid().toString());

            Element valueDomain = mdr.get(ElementDAO.class).getElement(((de.samply.mdr.dal.dto.DataElement) element).getValueDomainId());

            if(valueDomain instanceof de.samply.mdr.dal.dto.DescribedValueDomain) {
                de.samply.mdr.dal.dto.DescribedValueDomain descVd = (de.samply.mdr.dal.dto.DescribedValueDomain) valueDomain;

                DescribedValueDomain expVd = new DescribedValueDomain();
                expVd.setDatatype(descVd.getDatatype());
                expVd.setFormat(descVd.getFormat());
                expVd.setMaxCharacters(BigInteger.valueOf(descVd.getMaxCharacters()));
                expVd.setUnitOfMeasure(descVd.getUnitOfMeasure());
                expVd.setUuid(descVd.getUuid().toString());

                expVd.setDescription(descVd.getDescription());
                expVd.setValidationData(descVd.getValidationData());
                expVd.setValidationType(descVd.getValidationType().toString());

                add(factory.createDescribedValueDomain(expVd));

            } else if(valueDomain instanceof de.samply.mdr.dal.dto.EnumeratedValueDomain) {
                de.samply.mdr.dal.dto.EnumeratedValueDomain enumVd = (de.samply.mdr.dal.dto.EnumeratedValueDomain) valueDomain;

                EnumeratedValueDomain expVd = new EnumeratedValueDomain();
                expVd.setDatatype(enumVd.getDatatype());
                expVd.setFormat(enumVd.getFormat());
                expVd.setMaxCharacters(BigInteger.valueOf(enumVd.getMaxCharacters()));
                expVd.setUnitOfMeasure(enumVd.getUnitOfMeasure());
                expVd.setUuid(enumVd.getUuid().toString());

                List<Element> permissibleValues = mdr.get(ElementDAO.class).getPermissibleValues(valueDomain.getId());

                for(Element pvElement : permissibleValues) {
                    de.samply.mdr.dal.dto.PermissibleValue pv = (de.samply.mdr.dal.dto.PermissibleValue) pvElement;
                    PermissibleValue expValue = new PermissibleValue();

                    expValue.setUuid(pv.getUuid().toString());
                    expValue.setValueDomain(valueDomain.getUuid().toString());
                    expValue.setValue(pv.getPermittedValue());

                    exp.getDefinitions().getDefinition().addAll(
                            convert(mdr.get(DefinitionDAO.class).getDefinitions(pv.getId(),
                                        identified.getScoped().getId()), pv.getUuid()));

                    add(factory.createPermissibleValue(expValue));
                }

                add(factory.createEnumeratedValueDomain(expVd));
            } else if(valueDomain instanceof de.samply.mdr.dal.dto.CatalogValueDomain) {
                de.samply.mdr.dal.dto.CatalogValueDomain catVd = (de.samply.mdr.dal.dto.CatalogValueDomain) valueDomain;

                IdentifiedElement catalog = mdr.get(IdentifiedDAO.class).getElement(catVd.getCatalogScopedIdentifierId());

                CatalogValueDomain expVd = new CatalogValueDomain();
                expVd.setDatatype(catVd.getDatatype());
                expVd.setFormat(catVd.getFormat());
                expVd.setMaxCharacters(BigInteger.valueOf(catVd.getMaxCharacters()));
                expVd.setUnitOfMeasure(catVd.getUnitOfMeasure());
                expVd.setCatalog(catalog.getScoped().getUuid().toString());
                expVd.setUuid(catVd.getUuid().toString());

                List<IdentifiedElement> permissibleCodes = mdr.get(IdentifiedDAO.class).getPermissibleCodes(catVd);
                for(IdentifiedElement permissibleCode : permissibleCodes) {
                    expVd.getPermissibleCode().add(permissibleCode.getScoped().getUuid().toString());
                }

                add(factory.createCatalogValueDomain(expVd));

                export(catalog);
            }

            expEl.setValueDomain(valueDomain.getUuid().toString());

            add(factory.createDataElement(expEl));
        } else if(element instanceof de.samply.mdr.dal.dto.Record) {
            for(IdentifiedElement e : mdr.get(IdentifiedDAO.class).getSubMembers(identified.getScoped().toString())) {
                export(e);
                exp.getSub().add(e.getScoped().getUuid().toString());
            }

            Record expRecord = new Record();
            expRecord.setUuid(element.getUuid().toString());
            add(factory.createRecord(expRecord));
        } else if(element instanceof de.samply.mdr.dal.dto.Catalog) {
            for(IdentifiedElement e : mdr.get(IdentifiedDAO.class).getSubMembers(identified.getScoped().toString())) {
                export(e);

                exp.getSub().add(e.getScoped().getUuid().toString());
            }

            Catalog expCat = new Catalog();
            expCat.setUuid(element.getUuid().toString());
            add(factory.createCatalog(expCat));
        } else if(element instanceof de.samply.mdr.dal.dto.Code) {
            for(IdentifiedElement e : mdr.get(IdentifiedDAO.class).getSubMembers(identified.getScoped().toString())) {
                export(e);

                exp.getSub().add(e.getScoped().getUuid().toString());
            }

            de.samply.mdr.dal.dto.Code code = (de.samply.mdr.dal.dto.Code) element;

            Code expCode = new Code();
            expCode.setUuid(element.getUuid().toString());
            expCode.setIsValid(code.isValid());
            expCode.setCode(code.getCode());
            add(factory.createCode(expCode));
        }

        add(factory.createScopedIdentifier(exp));
    }

    /**
     * @param createDescribedValueDomain
     */
    private void add(JAXBElement<? extends de.samply.mdr.xsd.Element> element) {
        if(!exported.contains(UUID.fromString(element.getValue().getUuid()))) {
            target.getElement().add(element);
            exported.add(UUID.fromString(element.getValue().getUuid()));
        }
    }

    /**
     * @param def
     * @param uuid
     * @return
     */
    private Definition convert(de.samply.mdr.dal.dto.Definition def, UUID uuid) {
        Definition exp = new Definition();
        exp.setDefinition(def.getDefinition());
        exp.setDesignation(def.getDesignation());
        exp.setLang(def.getLanguage());
        exp.setUuid(uuid.toString());
        return exp;
    }

    private List<Definition> convert(List<de.samply.mdr.dal.dto.Definition> definitions, UUID uuid) {
        List<Definition> target = new ArrayList<>();
        for(de.samply.mdr.dal.dto.Definition def : definitions) {
            target.add(convert(def, uuid));
        }
        return target;
    }

    /**
     * @param ns
     * @throws DAOException
     */
    private void export(de.samply.mdr.dal.dto.Namespace ns) throws DAOException {
        Namespace expns = new Namespace();
        expns.setUuid(ns.getUuid().toString());
        expns.setName(ns.getName());

        DescribedElement descNs = mdr.get(NamespaceDAO.class).getNamespace(ns.getName());

        expns.setDefinitions(new Definitions());
        expns.getDefinitions().getDefinition().addAll(convert(descNs.getDefinitions(), ns.getUuid()));

        target.getElement().add(factory.createNamespace(expns));
        exported.add(ns.getUuid());
    }

    private de.samply.mdr.dal.dto.Namespace getNamespace(String namespace) throws DAOException {
        if(! namespaceMap.containsKey(namespace)) {
            namespaceMap.put(namespace, mdr.get(NamespaceDAO.class).getNamespace(namespace));
        }

        return (de.samply.mdr.dal.dto.Namespace) namespaceMap.get(namespace).getElement();
    }

}
