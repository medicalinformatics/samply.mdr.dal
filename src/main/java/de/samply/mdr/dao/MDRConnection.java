/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Misc;
import de.samply.mdr.dal.dto.Misc.ScopedHierarchy;
import de.samply.mdr.dal.dto.Misc.UserReadableNamespace;
import de.samply.mdr.dal.dto.Misc.UserWritableNamespace;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.NamespacePermission;
import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.ObjectMapper;
import de.samply.sdao.SQLFunction;
import de.samply.sdao.json.JSONResource;

/**
 * The Connection to the MDR PostgreSQL database.
 */
public class MDRConnection extends ConnectionProvider implements AutoCloseable {

    public static final int requiredVersion = 11;

    public MDRConnection(String host, String database, String username, String password, int userId) throws DAOException {
        super(host, database, username, password, userId);
    }

    /**
     * This method imports the given URN into the namespace "newNamespace".
     * It checks, if it already has been imported by using the uuid.
     * If it has not been imported yet, check if the same element has been imported
     * in a different version. If it has, create a new revision with the same identifier.
     * If it has never been imported, create a new identifier for the same element
     * @param urn
     * @param newNamespace
     * @param userId
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier importElement(String urn, String newNamespace, int userId) throws DAOException {
        return duplicate(urn, newNamespace, userId, false);
    }

    /**
     * Duplicates the specified URN.
     * @param urn
     * @param newNamespace
     * @param userId
     * @param existing if true, a new urn will be created, even if an identicial scoped identifier exists.
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier duplicate(String urn, String newNamespace, int userId, boolean existing) throws DAOException {
        if(!existing) {
            ScopedIdentifier importedIdentifier = get(ScopedIdentifierDAO.class).getIdenticalScopedIdentifier(urn, newNamespace, userId);
            if(importedIdentifier != null) {
                return importedIdentifier;
            }
        }

        IdentifiedElement element = get(IdentifiedDAO.class).getElement(urn);
        Namespace ns = get(ElementDAO.class).getNamespace(newNamespace);

        ScopedIdentifier identifier = element.getScoped();

        ScopedIdentifier newIdentifier = new ScopedIdentifier();
        newIdentifier.setCreatedBy(userId);
        newIdentifier.setNamespace(newNamespace);
        newIdentifier.setIdentifier(get(ScopedIdentifierDAO.class).getFreeIdentifier(newNamespace, identifier.getElementType()));
        newIdentifier.setVersion("1");
        newIdentifier.setElementId(identifier.getElementId());
        newIdentifier.setStatus(identifier.getStatus());
        newIdentifier.setUrl(identifier.toString());
        newIdentifier.setElementType(identifier.getElementType());
        newIdentifier.setNamespaceId(ns.getId());
        newIdentifier.setUuid(identifier.getUuid());

        if(!existing) {
            ScopedIdentifier importedIdentifier = get(ScopedIdentifierDAO.class).getScopedIdentifierForElement(urn, newNamespace, userId);
            if(importedIdentifier != null) {
                /**
                 * So the user has at some point already imported this element. Create a new
                 * revision of this Scoped Identifier and store it.
                 */
                newIdentifier.setVersion(nextRevision(importedIdentifier.getVersion()));
                newIdentifier.setIdentifier(importedIdentifier.getIdentifier());
            }
        }

        get(ScopedIdentifierDAO.class).saveScopedIdentifier(newIdentifier);

        for(Slot s : get(SlotDAO.class).getSlots(identifier.toString())) {
            Slot newSlot = new Slot();
            newSlot.setKey(s.getKey());
            newSlot.setValue(s.getValue());
            newSlot.setScopedIdentifierId(newIdentifier.getId());
            get(SlotDAO.class).saveSlot(newSlot);
        }

        for(Definition definition : element.getDefinitions()) {
            Definition newDefinition = new Definition();
            newDefinition.setDefinition(definition.getDefinition());
            newDefinition.setDesignation(definition.getDesignation());
            newDefinition.setElementId(definition.getElementId());
            newDefinition.setLanguage(definition.getLanguage());
            newDefinition.setScopedIdentifierId(newIdentifier.getId());

            get(DefinitionDAO.class).saveDefinition(newDefinition);
        }

        if(identifier.getElementType() == ElementType.DATAELEMENT) {
            DataElement dataElement = (DataElement) get(ElementDAO.class).getElement(identifier.getElementId());
            ValueDomain domain = (ValueDomain) get(ElementDAO.class).getElement(dataElement.getValueDomainId());

            /**
             * If it is an enumerated value domain, copy every definition into the new namespace
             */
            if(domain instanceof EnumeratedValueDomain) {
                List<Element> values = get(ElementDAO.class).getPermissibleValues(domain.getId());
                for(Element value : values) {
                    List<Definition> definitions = get(DefinitionDAO.class)
                            .getDefinitions(value.getId(), identifier.getId());

                    for(Definition definition : definitions) {
                        Definition newDefinition = new Definition();
                        newDefinition.setDefinition(definition.getDefinition());
                        newDefinition.setDesignation(definition.getDesignation());
                        newDefinition.setElementId(definition.getElementId());
                        newDefinition.setLanguage(definition.getLanguage());
                        newDefinition.setScopedIdentifierId(newIdentifier.getId());

                        get(DefinitionDAO.class).saveDefinition(newDefinition);
                    }
                }
            }
        }

        return newIdentifier;
    }

    /**
     * Returns all hierarchy nodes for the given URN.
     * @param urn
     * @return
     * @throws DAOException
     */
    public List<HierarchyNode> getHierarchyNodes(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        return executeSelect("SELECT " + getSelectFields(HierarchyNode.class) + " FROM " + HierarchyNode.TABLE.from() + " LEFT JOIN "
                + ScopedIdentifier.TABLE.from() + " ON " + ScopedIdentifier.ID.access() + " = " + HierarchyNode.ROOT.access()
                + " LEFT JOIN " + Misc.NS.from() + " ON "
                + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                + ScopedIdentifier.IDENTIFIER.access() + "= ? AND "
                + ScopedIdentifier.VERSION.access() + "= ? AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                + Misc.NAME.access() + "= ?", hierarchyMapper, identifier.getIdentifier(), identifier.getVersion(),
                identifier.getElementType(), identifier.getNamespace());
    }

    /**
     * Returns the next revision for the given version.
     * @param version
     * @return
     */
    private String nextRevision(String version) {
        try {
            return "" + (Integer.parseInt(version) + 1);
        } catch(Exception e) {
            return "1";
        }
    }

    /**
     * Redirects the given Scoped Identifier to another element. Please note, that all previously stored definitions, slots
     * and hierarchy objects will be deleted.
     * @param identifier
     * @throws DAOException
     */
    public void updateScopedIdentifier(ScopedIdentifier identifier) throws DAOException {
        ScopedIdentifier realIdentifier = get(ScopedIdentifierDAO.class).getScopedIdentifier(identifier.toString());

        delete(Definition.TABLE, Definition.SCOPED_ID.val(realIdentifier.getId()));
        delete(ScopedHierarchy.TABLE, ScopedHierarchy.SUPER.val(realIdentifier.getId()));
        delete(Slot.TABLE, Slot.SCOPED_ID.val(realIdentifier.getId()));

        update(ScopedIdentifier.TABLE, ScopedIdentifier.ID.val(realIdentifier.getId()),
                ScopedIdentifier.ELEMENT_ID.val(identifier.getElementId()),
                ScopedIdentifier.STATUS.val(identifier.getStatus()),
                ScopedIdentifier.UUID.val(new SQLFunction("DEFAULT")));
    }

    /**
     * Returns a list of Namespace Permissions.
     * @param namespace
     * @return
     * @throws DAOException
     */
    public List<NamespacePermission> getPermissions(Namespace namespace) throws DAOException {
        HashMap<Integer, Permission> map = new HashMap<>();

        List<NamespacePermission> readable = executeSelect("SELECT " + getSelectFields(UserReadableNamespace.class)+ " FROM "
                + UserReadableNamespace.TABLE.from() + " WHERE "
                + UserReadableNamespace.NAMESPACE_ID.access() + " = ?", permissionMapper, namespace.getId());

        List<NamespacePermission> writable = executeSelect("SELECT " + getSelectFields(UserWritableNamespace.class)+ " FROM "
                + UserWritableNamespace.TABLE.from() + " WHERE "
                + UserWritableNamespace.NAMESPACE_ID.access() + " = ?", permissionMapper, namespace.getId());

        for(NamespacePermission p : readable) {
            map.put(p.getUserId(), Permission.READ);
        }

        for(NamespacePermission p : writable) {
            map.put(p.getUserId(), Permission.READ_WRITE);
        }

        ArrayList<NamespacePermission> target = new ArrayList<>();

        for(Entry<Integer, Permission> entry : map.entrySet()) {
            NamespacePermission permission = new NamespacePermission();
            permission.setNamespaceId(namespace.getId());
            permission.setUserId(entry.getKey());
            permission.setPermission(entry.getValue());
            target.add(permission);
        }

        return target;
    }


    /**
     * Updated the permissions do the given namespace.
     * @param permissions
     * @param ns
     * @throws DAOException
     */
    public void updatePermissions(List<NamespacePermission> permissions, Namespace ns) throws DAOException {
        delete(UserReadableNamespace.TABLE, UserReadableNamespace.NAMESPACE_ID.val(ns.getId()));
        delete(UserWritableNamespace.TABLE, UserReadableNamespace.NAMESPACE_ID.val(ns.getId()));

        for(NamespacePermission permission : permissions) {
            insert(UserReadableNamespace.TABLE,
                    UserReadableNamespace.USER_ID.val(permission.getUserId()),
                    UserReadableNamespace.NAMESPACE_ID.val(ns.getId()));

            if(permission.getPermission() == Permission.READ_WRITE) {
                insert(UserWritableNamespace.TABLE,
                        UserWritableNamespace.USER_ID.val(permission.getUserId()),
                        UserWritableNamespace.NAMESPACE_ID.val(ns.getId()));
            }
        }
    }

    /**
     * Checks the database version and returns true, if they are compatible.
     * @return
     */
    public boolean checkDbVersion() {
        try {
            JSONResource config = getConfig();

            if(config.getProperty(Vocabulary.DB_VERSION).asInteger() != requiredVersion) {
                return false;
            }

            return true;
        } catch(Exception e) {
            throw new Error("Database Version is not compatible with this library!");
        }
    }

    /**
     * Refreshes all materialized views.
     * @throws DAOException
     */
    public void refreshMaterializedViews() throws DAOException {
        for(String view : materializedViews) {
            StringBuilder sql = new StringBuilder("REFRESH MATERIALIZED VIEW ").append(view);
            executeUpdate(sql.toString());
        }
    }

    @Override
    public int getRequiredVersion() throws DAOException {
        return requiredVersion;
    }

    /**
     * @return the userId
     */
    @Override
    public int getUserId() {
        return userId;
    }

    /**
     * Cleanup the database. Removes old ScopedIdentifier for codes.
     * @throws DAOException
     */
    public void cleanup() throws DAOException {
        executeUpdate("DELETE FROM " + ScopedIdentifier.TABLE.from() + " WHERE "
                + ScopedIdentifier.STATUS.access() + " = ?::\"status\"  AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                + ScopedIdentifier.ID.access() + " NOT IN "
                        + "(SELECT " + HierarchyNode.SUB.access() + " FROM "
                                + HierarchyNode.TABLE.from() + ")", Status.DRAFT, ElementType.CODE);
    }

    private static final String[] materializedViews = new String[] {HierarchyNode.TABLE.table()};

    /**
     * The object mapper for namespace permissions.
     */
    private static final ObjectMapper<NamespacePermission> permissionMapper = new ObjectMapper<NamespacePermission>() {
        @Override
        public NamespacePermission getObject(ResultSet set)
                throws SQLException {
            NamespacePermission permission = new NamespacePermission();
            permission.setUserId(set.getInt(UserReadableNamespace.USER_ID.alias));
            permission.setNamespaceId(set.getInt(UserReadableNamespace.NAMESPACE_ID.alias));
            return permission;
        }
    };

    /**
     * The mapper for the hierarchy nodes.
     */
    private ObjectMapper<HierarchyNode> hierarchyMapper = new ObjectMapper<HierarchyNode>() {
        @Override
        public HierarchyNode getObject(ResultSet set) throws SQLException {
            HierarchyNode node = new HierarchyNode();
            node.setSubId(set.getInt(HierarchyNode.SUB.alias));
            node.setSuperId(set.getInt(HierarchyNode.SUPER.alias));
            return node;
        }
    };

}
