/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A slot (a key value pair)
 * @author paul
 *
 */
public class Slot implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2922421882685775421L;

    public static final Table TABLE = new Table("slot", "sl");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    private String key;
    public static final Column KEY = new Column(TABLE, "key");

    private String value;
    public static final Column VALUE = new Column(TABLE, "value");

    private int scopedIdentifierId;
    public static final Column SCOPED_ID = new Column(TABLE, "scopedIdentifierId");

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getScopedIdentifierId() {
        return scopedIdentifierId;
    }

    public void setScopedIdentifierId(int scopedIdentifierId) {
        this.scopedIdentifierId = scopedIdentifierId;
    }

}
