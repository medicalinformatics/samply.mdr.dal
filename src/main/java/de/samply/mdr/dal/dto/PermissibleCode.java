/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A permissible code in a catalog value domain.
 */
public class PermissibleCode implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("permissibleCode", "pc");

    /**
     * The catalog value domains ID.
     */
    private int catalogValueDomainId;
    public static final Column CATALOG_VD_ID = new Column(TABLE, "catalogValueDomainId");

    /**
     * The codes scoped identifier ID.
     */
    private int codeScopedIdentifierId;
    public static final Column CODE_SCOPED_ID = new Column(TABLE, "codeScopedIdentifierId");

    /**
     * @return the catalogValueDomainId
     */
    public int getCatalogValueDomainId() {
        return catalogValueDomainId;
    }

    /**
     * @param catalogValueDomainId the catalogValueDomainId to set
     */
    public void setCatalogValueDomainId(int catalogValueDomainId) {
        this.catalogValueDomainId = catalogValueDomainId;
    }

    /**
     * @return the codeScopedIdentifierId
     */
    public int getCodeScopedIdentifierId() {
        return codeScopedIdentifierId;
    }

    /**
     * @param codeScopedIdentifierId the codeScopedIdentifierId to set
     */
    public void setCodeScopedIdentifierId(int codeScopedIdentifierId) {
        this.codeScopedIdentifierId = codeScopedIdentifierId;
    }

}
