/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import de.samply.sdao.definition.Column;

/**
 * A code from a catalog, e.g. "C00.1" from the ICD-10 code.
 */
public class Code extends Element {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int catalogId; // ELEMENT_ID

    /**
     * The code itself, e.g. "C00.1"
     */
    private String code;
    public static final Column CODE = new Column(TABLE, "code");

    /**
     * If false, this code is just a hierarchical node and must not be used as
     * a valid code for user selection. Chapters and Blocks from the ICD-10 catalog
     * are not valid codes, but hierarchical nodes.
     */
    private boolean isValid;
    public static final Column IS_VALID = new Column(TABLE, "isValid");

    /**
     * @return the catalogId
     */
    public int getCatalogId() {
        return catalogId;
    }

    /**
     * @param catalogId the catalogId to set
     */
    public void setCatalogId(int catalogId) {
        this.catalogId = catalogId;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the isValid
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * @param isValid the isValid to set
     */
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

}
