/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;
import java.util.UUID;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

/**
 * A scoped identifier gives an identified item a unique identifier plus version codes and other attributes
 * @author paul
 *
 */
public class ScopedIdentifier implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1750623556646093368L;

    public static final Table TABLE = new Table("scopedIdentifier", "sc");

    /**
     * The ScopedIdentifiers ID
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The version as a string (e.g. "1.3")
     */
    private String version;
    public static final Column VERSION = new Column(TABLE, "version");

    /**
     * The identifier, e.g. "3452431a
     */
    private String identifier;
    public static final Column IDENTIFIER = new Column(TABLE, "identifier");

    /**
     * Not used at this moment
     */
    private String url;
    public static final Column URL = new Column(TABLE, "url");

    /**
     * The element type
     */
    private ElementType elementType;
    public static final Column ELEMENT_TYPE = new Column(TABLE, "elementType");

    /**
     * The real namespace name
     */
    private String namespace;
    public static final Column NAMESPACE = Misc.NAME;

    /**
     * The users ID who created this scoped identifier;
     */
    private int createdBy;
    public static final Column CREATED_BY = new Column(TABLE, "createdBy");

    /**
     * The scoped identifiers "uuid"
     */
    private UUID uuid;
    public static final Column UUID = new Column(TABLE, "uuid");

    /**
     * The namespcae in which this scoped identifier belongs
     */
    private int namespaceId;
    public static final Column NAMESPACE_ID = new Column(TABLE, "namespaceId");

    /**
     * The identified item ID
     */
    private int elementId;
    public static final Column ELEMENT_ID = new Column(TABLE, "elementId");

    /**
     * Indicates the status of this scoped identifier
     */
    private Status status;
    public static final Column STATUS = new Column(TABLE, "status");


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getNamespaceId() {
        return namespaceId;
    }

    public void setNamespaceId(int namespaceId) {
        this.namespaceId = namespaceId;
    }

    public int getElementId() {
        return elementId;
    }

    public void setElementId(int identifiedId) {
        this.elementId = identifiedId;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ScopedIdentifier)) {
            return false;
        }
        ScopedIdentifier i = (ScopedIdentifier) obj;
        return i.id == id && i.elementId == elementId && i.status == status && i.identifier.equals(identifier)
                && i.namespaceId == namespaceId && i.url.equals(url) && i.version.equals(version) && elementType == i.elementType;
    }

    public boolean isFirstDraft() {
        return status == Status.DRAFT && "1".equals(version);
    }

    @Override
    public String toString() {
        return "urn:" + namespace + ":" + elementType.toString().toLowerCase() + ":" + identifier + ":" + version;
    }

    public ElementType getElementType() {
        return elementType;
    }

    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }

    public static ScopedIdentifier fromURN(String urn) {
        ScopedIdentifier identifier = new ScopedIdentifier();

        if(!isURN(urn)) {
            throw new IllegalArgumentException();
        }

        String[] parts = urn.split(":");

        identifier.namespace = parts[1];
        identifier.setElementType(ElementType.valueOf(parts[2].toUpperCase()));
        identifier.setIdentifier(parts[3]);
        identifier.setVersion(parts[4]);
        return identifier;
    }

    public static boolean isURN(String urn) {
        if(urn == null) {
            return false;
        }

        String[] parts = urn.split(":");

        if(parts.length != 5 || !parts[0].equals("urn")) {
            return false;
        } else {
            return true;
        }
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
