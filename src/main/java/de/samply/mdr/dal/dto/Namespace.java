/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import de.samply.mdr.dao.Vocabulary;
import de.samply.sdao.definition.Column;
import de.samply.sdao.json.JSONResource;

/**
 * The namespace inside a MDR source
 * @author paul
 *
 */
public class Namespace extends Element {

    /**
     *
     */
    private static final long serialVersionUID = 3341526117020615061L;

    /**
     * The name
     */
    private String name;
    public static final Column NAME = new Column(TABLE, "name");

    /**
     * The users ID, who has created this namespace
     */
    private int createdBy;
    public static final Column CREATED_BY = new Column(TABLE, "createdBy");

    /**
     * If the namespace is hidden, it is only visible to certain users
     */
    private Boolean hidden = false;
    public static final Column HIDDEN = new Column(TABLE, "hidden");

    /**
     * The additional data column, e.g. constraints.
     */
    private JSONResource data = new JSONResource();
    public static final Column DATA = new Column(TABLE, "data");

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Namespace)) {
            return false;
        }
        Namespace n = (Namespace) obj;
        return super.equals(obj) && n.name.equals(name);
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return the data
     */
    public JSONResource getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(JSONResource data) {
        this.data = data;
    }

    public JSONResource getConstraints() {
        if(data.getDefinedProperties().contains(Vocabulary.CONSTRAINTS)) {
            return (JSONResource) data.getProperty(Vocabulary.CONSTRAINTS);
        } else {
            return new JSONResource();
        }
    }

}
