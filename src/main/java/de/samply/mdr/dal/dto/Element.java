/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;
import java.util.UUID;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public abstract class Element implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("element", "e");

    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * The element type of this element.
     */
    private ElementType elementType;
    public static final Column ELEMENT_TYPE = new Column(TABLE, "elementType");

    /**
     * A reference to another element, e.g. a value domain for a dataelement, the catalog for a code,
     * the valuedomain for a permissible value. This column does not have
     * an attribute in this class because it might be used differently in some cases.
     */
    public static final Column ELEMENT_ID = new Column(TABLE, "elementId");

    /**
     * A reference to a scoped identifier, e.g. a catalog. This column does not have
     * an attribute in this class because it might be used differently in some cases.
     */
    public static final Column SCOPED_ID = new Column(TABLE, "scopedIdentifierId");

    /**
     * The UUID of this element
     */
    private UUID uuid;
    public static final Column UUID = new Column(TABLE, "uuid");

    /**
     * The external ID of this element, e.g. "cadsr://dataelement?publicId=abc"
     */
    private String externalId;
    public static final Column EXTERNAL_ID = new Column(TABLE, "externalId");

    /**
     * @return the elementType
     */
    public ElementType getElementType() {
        return elementType;
    }

    /**
     * @param elementType the elementType to set
     */
    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Element)) {
            return false;
        }
        Element e = (Element) obj;
        return id == e.id;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Integer.valueOf(id).hashCode();
    }

    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the externalId
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

}
