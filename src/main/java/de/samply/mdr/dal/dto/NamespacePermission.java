/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;

/**
 * A namespace permission grants a user access to a certain
 * namespace. The mode of access is described by the permission field.
 */
public class NamespacePermission implements Serializable {

    /**
     * The permission enum that describes the mode of access.
     */
    public enum Permission {
        READ, READ_WRITE;

        public String getName() {
            return toString();
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The users id
     */
    private int userId;

    /**
     * The namespaces ID.
     */
    private int namespaceId;

    /**
     * The mode of access.
     */
    private Permission permission;

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the permission
     */
    public Permission getPermission() {
        return permission;
    }

    /**
     * @param permission the permission to set
     */
    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    /**
     * @return the namespaceId
     */
    public int getNamespaceId() {
        return namespaceId;
    }

    /**
     * @param namespaceId the namespaceId to set
     */
    public void setNamespaceId(int namespaceId) {
        this.namespaceId = namespaceId;
    }

}
