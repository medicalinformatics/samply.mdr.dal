# Changelog Samply MDR Data Access Layer

## Version 1.9.1

- added configuration option to show/hide automatic translation service for definitions

## Version 1.9.0

- added the findOwnedRepresentations method that returns a list of the owned
    dataelements that described the same element as the given one
- added support for external IDs (e.g. for the caDSR)
- switched to the open source repository

## Version 1.8.0
- polished the DAOs

## Version 1.7.0

- Added a new config value: if "defaultGrant" is true, new users can create
  namespaces without explicit permission from the admin
- Added a new util used to update groups and all their sub elements
- Added support for catalogs and codes
- Restructered all classes. Removed the "identifiedItem" and "designatable" and
  combined them into one "element". There are now different kinds of elements:
  - namespace
  - dataelement
  - dataelementgroup
  - record
  - catalog
  - code
  - enumerated value domain
  - described value domain
  - catalog value domain
  - permissible value
  The SQL tables are now build using the "Table per hierarchy" method, meaning that
  all elements are stored in one table with many columns. This should result in
  faster queries because of fewer joins. This change also reduced the number of
  DAOs used. Use the ElementDAO to get elements (meaning the element without
  using a scoped identifier, thus without definitions), use the IdentifiedDAO to
  get namespaces (because they are somewhat special) and elements using a
  scoped identifer.

