# Samply MDR DAL

The Samply MDR Data Access Layer is a library that must be used to get elements from the MDR database.
It handles access rights accordingly to the database and user.


The layer distincts between:

- Elements (Dataelements, Dataelementgroup, Namespace, Record, Value Domain, etc.)
- Identified Elements (Scoped Identifier + Element + Definition/Designation + Slots)
- Described Elements (Namespaces + Definition/Designation


## Build

Use maven to build the `war` file:

```
mvn clean package
```
